import { expect } from "chai";
import { describe, it } from "mocha";
import { ProductionResourceReqDto, SimultaneousProductionConditionType, isProductionResourceReqDto } from './production-resource.req-dto';

const exampleData: ProductionResourceReqDto = {
  id: 'id',
  producibleMaterials: [
    {
      materialGroup: 'group1',
      timeFunction: 123,
      costFunction: 456,
    }
  ],
  bufferIdBefore: 'buffer1',
  bufferIdAfter: 'buffer2',
  simultaneousProductionSlots: 1,
  simultaneousProductionConditions: [
    {
      type: SimultaneousProductionConditionType.MATERIAL_EXCLUSIONS,
      value1: 'm1',
      value2: 'm2',
    },
    {
      type: SimultaneousProductionConditionType.SAME_START_END_TIMES,
    },
  ],
  changeovers: [
    {
      fromMaterialGroup: 'group1',
      toMaterialGroup: 'group2',
      timeFunction: 789,
      costFunction: 123,
    },
  ],
  fixedAssignments: [
    {
      fromTime: 0,
      toTime: 100,
      startedOperationsCanBeSplit: true,
    },
  ]
};

describe('isProductionResourceReqDto', () => {
  it('should work', () => {
    expect(isProductionResourceReqDto(exampleData)).to.be.true;
  })
  it('should fail with missing values', () => {
    for (const k of Object.keys(exampleData)) {
      const tmp:any = {...exampleData};
      delete tmp[k]
      expect(isProductionResourceReqDto(tmp)).to.be.false;
    }
  })
  it('should fail with wrong attribute values', () => {
    for (const k of Object.keys(exampleData)) {
      const tmp:any = {...exampleData};
      tmp[k] = null
      expect(isProductionResourceReqDto(tmp)).to.be.false;
    }
  })
  it('should fail with wrong value as input', () => {
    expect(isProductionResourceReqDto(123)).to.be.false
  })
})
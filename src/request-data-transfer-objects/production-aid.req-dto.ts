
export interface ProductionAidReqDto {
  productionAidId: string;
  amountAvailable: number;
}

export function isProductionAidReqDto(v: unknown): v is ProductionAidReqDto {
  return (
    typeof v === 'object' && v !== null &&
    'productionAidId' in v && typeof v.productionAidId === 'string' &&
    'amountAvailable' in v && typeof v.amountAvailable === 'number'
  )
}
import { expect } from "chai";
import { describe, it } from "mocha";
import { ProductionAidReqDto, isProductionAidReqDto } from "./production-aid.req-dto";

const exampleData: ProductionAidReqDto = {
  productionAidId: 'id',
  amountAvailable: 12,
};

describe('isProductionAidReqDto', () => {
  it('should work', () => {
    expect(isProductionAidReqDto(exampleData)).to.be.true;
  })
  it('should fail with missing values', () => {
    for (const k of Object.keys(exampleData)) {
      const tmp:any = {...exampleData};
      delete tmp[k]
      expect(isProductionAidReqDto(tmp)).to.be.false;
    }
  })
  it('should fail with wrong attribute values', () => {
    for (const k of Object.keys(exampleData)) {
      const tmp:any = {...exampleData};
      tmp[k] = null
      expect(isProductionAidReqDto(tmp)).to.be.false;
    }
  })
  it('should fail with wrong value as input', () => {
    expect(isProductionAidReqDto(123)).to.be.false
  })
})
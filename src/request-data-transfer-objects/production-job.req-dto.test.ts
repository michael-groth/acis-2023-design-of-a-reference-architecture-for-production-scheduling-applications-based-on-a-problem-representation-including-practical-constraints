import { expect } from "chai";
import { describe, it } from "mocha";
import { ProductionJobReqDto, isProductionJobReqDto } from "./production-job.req-dto";

const exampleData: ProductionJobReqDto = {
  name: 'name',
  dueDate: 1000,
  priority: 1,
  operations: [],
};

describe('isProductionJobReqDto', () => {
  it('should work', () => {
    expect(isProductionJobReqDto(exampleData)).to.be.true;
  })
  it('should fail with missing values', () => {
    for (const k of Object.keys(exampleData)) {
      const tmp:any = {...exampleData};
      delete tmp[k]
      expect(isProductionJobReqDto(tmp)).to.be.false;
    }
  })
  it('should fail with wrong attribute values', () => {
    for (const k of Object.keys(exampleData)) {
      const tmp:any = {...exampleData};
      tmp[k] = null
      expect(isProductionJobReqDto(tmp)).to.be.false;
    }
  })
  it('should fail with wrong value as input', () => {
    expect(isProductionJobReqDto(123)).to.be.false
  })
})
import { expect } from "chai";
import { describe, it } from "mocha";
import { BufferReqDto, isBufferReqDto } from "./buffer.req-dto";

const exampleData: BufferReqDto = {
  bufferId: 'bufferid',
  size: 1,
};

describe('isBufferReqDto', () => {
  it('should work', () => {
    expect(isBufferReqDto(exampleData)).to.be.true;
  })
  it('should fail with missing values', () => {
    for (const k of Object.keys(exampleData)) {
      const tmp:any = {...exampleData};
      delete tmp[k]
      expect(isBufferReqDto(tmp)).to.be.false;
    }
  })
  it('should fail with wrong attribute values', () => {
    for (const k of Object.keys(exampleData)) {
      const tmp:any = {...exampleData};
      tmp[k] = null
      expect(isBufferReqDto(tmp)).to.be.false;
    }
  })
  it('should fail with wrong value as input', () => {
    expect(isBufferReqDto(123)).to.be.false
  })
})
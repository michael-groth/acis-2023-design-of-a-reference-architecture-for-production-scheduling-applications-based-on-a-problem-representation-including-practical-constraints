
export interface SimultaneousProductionConditionReqDto {
  /**
   * all the ids of the production resources for that the rule apply0
   */
  productionResourceIds: string[];

  type: SimultaneousProductionConditionType;

  value1?: string;
  value2?: string;

}

export enum SimultaneousProductionConditionType {
  SAME_START_END_TIMES = 'SAME_START_END_END_TIMES',
  /**
   * two materials that cannot be produced at the same time
   * value1 = material group 1
   * value2 = material group 2
   */
  MATERIAL_EXCLUSIONS = 'MATERIAL_EXCLUSIONS',
}

export function isSimultaneousProductionConditionReqDto(v: any): v is SimultaneousProductionConditionReqDto {
  return (
    typeof v === 'object' && v !== null &&
    'productionResourceIds' in v && Array.isArray(v.productionResourceIds) && v.productionResourceIds.every((i: unknown) => {
      typeof i === 'string'
    }) &&
    'type' in v && typeof v.type === 'string' && Object.values(SimultaneousProductionConditionType).includes(v.type) &&
    (v.value1  === undefined || ('value1' in v && typeof v.value1 === 'string')) &&
    (v.value2 === undefined || ('value2' in v && typeof v.value2 === 'string'))
  )
}
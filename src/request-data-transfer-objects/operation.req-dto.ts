
export interface OperationReqDto {
  /**
   * identifier of an operation
   */
  operationId: string;

  /**
   * the amount of the resulted materials
   */
  amount: number;

  /**
   * array of possible outcome material ids. One of those must be produced
   */
  producesMaterial: string[];

  /** Required for initial buffer fill calculation */
  madeFromMaterial: string;

  /**
   * predecessors
   */
  predecessors: {
    operationId: string;
    successorHasNoWaitCondition: boolean;
  }[];

  /**
   * defines if a previous operation, that is not in the scope of the schedule creation request, filled a buffer. The buffer is emptied by scheduling this operation
   */
  initialBufferFill?: {
    bufferId: string;
    bufferUsage: number;
  }

  /**
   * defines how long the resulting material has to be stored before processing can continue, defaults to 0
   */
  minimumStoragePeriod?: number;

  requiredProductionAids?: {
    productionAidId: string;
    amount: number;
  }[];
}

export function isOperationReqDto(v: unknown): v is OperationReqDto {
  return (
    typeof v === 'object' && v !== null &&
    'operationId' in v && typeof v.operationId === 'string' && 
    'amount' in v && typeof v.amount === 'number' &&
    'producesMaterial' in v && Array.isArray(v.producesMaterial) && v.producesMaterial.every(i => typeof i === 'string') &&
    'madeFromMaterial' in v && Array.isArray(v.madeFromMaterial) && v.madeFromMaterial.every(i => typeof i === 'string') &&
    'predecessors' in v && Array.isArray(v.predecessors) && v.predecessors.every(
      i => typeof i === 'object' && i !== null &&
      'operationId' in i && typeof i.operationId ==='string' && 
     'successorHasNoWaitCondition' in i && typeof i.successorHasNoWaitCondition === 'boolean'
    ) &&
    (!('initialBufferFill' in v) || ('initialBufferFill' in v && (
      typeof v.initialBufferFill === 'object' && v.initialBufferFill !== null &&
      'bufferId' in v.initialBufferFill && typeof v.initialBufferFill.bufferId === 'string' &&
      'bufferUsage' in v.initialBufferFill && typeof v.initialBufferFill.bufferUsage === 'number'
    ))) &&
    (!('requiredProductionAids' in v) || ('requiredProductionAids' in v && (
      typeof v.requiredProductionAids === 'object' && v.requiredProductionAids !== null &&
      'productionAidId' in v.requiredProductionAids && typeof v.requiredProductionAids.productionAidId === 'string' &&
      'amount' in v.requiredProductionAids && typeof v.requiredProductionAids.amount === 'number'
    )))
  )
}
import { expect } from "chai";
import { describe, it } from "mocha";
import { MaterialReqDto, isMaterialReqDto } from "./material.req-dto";

const exampleData: MaterialReqDto = {
  materialId: 'materialid',
  name: 'nameofmaterial',
  bufferUsage: 1,
  materialGroups: ['group1', 'group2'],
};

describe('isMaterialReqDto', () => {
  it('should work', () => {
    expect(isMaterialReqDto(exampleData)).to.be.true;
  })
  it('should fail with missing values', () => {
    for (const k of Object.keys(exampleData)) {
      const tmp:any = {...exampleData};
      delete tmp[k]
      expect(isMaterialReqDto(tmp)).to.be.false;
    }
  })
  it('should fail with wrong attribute values', () => {
    for (const k of Object.keys(exampleData)) {
      const tmp:any = {...exampleData};
      tmp[k] = null
      expect(isMaterialReqDto(tmp)).to.be.false;
    }
  })
  it('should fail with wrong value as input', () => {
    expect(isMaterialReqDto(123)).to.be.false
  })
})
import { expect } from "chai";
import { describe, it } from "mocha";
import { SchedulingProblemReqDto, isSchedulingProblemReqDto } from "./scheduling-problem.req-dto";

const exampleSchedulingProblem: SchedulingProblemReqDto = {
  goalFunction: 123,
  jobs: [],
  materials: [],
  productionResources: [],
  productionAids: [],
  buffers: [],
}

describe('SchedulingProblemDto', () => {
  it('should work', () => {
    expect(isSchedulingProblemReqDto(exampleSchedulingProblem)).to.be.true;
  })
  it('should fail with missing values', () => {
    for (const k of Object.keys(exampleSchedulingProblem)) {
      const tmp:any = {...exampleSchedulingProblem};
      delete tmp[k]
      expect(isSchedulingProblemReqDto(tmp)).to.be.false;
    }
  })
  it('should fail with wrong attribute values', () => {
    for (const k of Object.keys(exampleSchedulingProblem)) {
      const tmp:any = {...exampleSchedulingProblem};
      tmp[k] = null
      expect(isSchedulingProblemReqDto(tmp)).to.be.false;
    }
  })
  it('should fail with wrong value as input', () => {
    expect(isSchedulingProblemReqDto(123)).to.be.false
  })
})

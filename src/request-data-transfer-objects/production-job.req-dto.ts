import { OperationReqDto, isOperationReqDto } from "./operation.req-dto";

export interface ProductionJobReqDto {

  /**
   * 0= scheduling moment
   */
  dueDate: number;

  /**
   * 1 = normal priority, higher = higher priority
   */
  priority: number;

  /**
   * Name to use in results
   */
  name: string;

  /**
   * earliest start date, 0=scheduling moment, default: 0
   */
  releaseDate?: number;

  /**
   * the list of operations to be completed to count this job to be completed
   */
  operations: OperationReqDto[];
}

export function isProductionJobReqDto(v: unknown): v is OperationReqDto {
  return (
    typeof v === "object" && v !== null &&
    'dueDate' in v && typeof v.dueDate === "number" &&
    'priority' in v && typeof v.priority === "number" &&
    'name' in v && typeof v.name === "string" &&
    (!('releaseDate' in v) || ('releaseDate' in v && typeof v.releaseDate === 'number')) &&
    'operations' in v && Array.isArray(v.operations) && v.operations.every(i => isOperationReqDto(i))
  )
}
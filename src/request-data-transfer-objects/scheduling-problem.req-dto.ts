import { FunctionProvider, isFunctionProvider } from "../function-evaluator/function-provider.interface";
import { BufferReqDto, isBufferReqDto } from "./buffer.req-dto";
import { MaterialReqDto, isMaterialReqDto } from "./material.req-dto";
import { ProductionAidReqDto, isProductionAidReqDto } from "./production-aid.req-dto";
import { ProductionJobReqDto, isProductionJobReqDto } from "./production-job.req-dto";
import { ProductionResourceReqDto, isProductionResourceReqDto } from "./production-resource.req-dto";
import { SimultaneousProductionConditionReqDto, isSimultaneousProductionConditionReqDto } from "./simultaneous-production-condition.req-dto";

export interface SchedulingProblemReqDto {
  goalFunction: FunctionProvider;
  
  jobs: ProductionJobReqDto[];

  materials: MaterialReqDto[];

  simultaneousProductionConditions: SimultaneousProductionConditionReqDto[];

  productionResources: ProductionResourceReqDto[];

  productionAids: ProductionAidReqDto[];

  buffers: BufferReqDto[];
}

export function isSchedulingProblemReqDto(v: unknown): v is SchedulingProblemReqDto {
  return (
    typeof v === "object" && v !== null &&
    'goalFunction' in v && isFunctionProvider(v.goalFunction) &&
    'jobs' in v && Array.isArray(v.jobs) && v.jobs.every(i => isProductionJobReqDto(i)) &&
    'materials' in v && Array.isArray(v.materials) && v.materials.every(i => isMaterialReqDto(i)) &&
    'simultaneousProductionConditions' in v && Array.isArray(v.simultaneousProductionConditions) && v.simultaneousProductionConditions.every(i => isSimultaneousProductionConditionReqDto(i)) &&
    'productionResources' in v && Array.isArray(v.productionResources) && v.productionResources.every(i => isProductionResourceReqDto(i)) &&
    'productionAids' in v && Array.isArray(v.productionAids) && v.productionAids.every(i => isProductionAidReqDto(i)) &&
    'buffers' in v && Array.isArray(v.buffers) && v.buffers.every(i => isBufferReqDto(i))
  )
}
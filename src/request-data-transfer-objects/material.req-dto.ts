
export interface MaterialReqDto {
  /** identifier of the material */
  materialId: string;

  /** only used for result */
  name: string;

  bufferUsage: number;

  /** list of material group ids */
  materialGroups: string[];
}

export function isMaterialReqDto(v: unknown): v is MaterialReqDto {
  return (
    typeof v === 'object' && v !== null &&
    'materialId' in v && typeof v.materialId === 'string' &&
    'name' in v && typeof v.name === 'string' &&
    'bufferUsage' in v && typeof v.bufferUsage === 'number' &&
    'materialGroups' in v && Array.isArray(v.materialGroups) && v.materialGroups.every(i => typeof i === 'string')
  )
}
import { FunctionProvider, isFunctionProvider } from "../function-evaluator/function-provider.interface";

export interface ProductionResourceReqDto {
  id: string;

  /** list of all materials that can be produced on this production resource */
  producibleMaterials: {
    materialGroup: string;
    timeFunction: FunctionProvider;
    costFunction: FunctionProvider;
  }[];

  bufferIdBefore: string;

  bufferIdAfter: string;

  changeovers: {
    fromMaterialGroup: string;
    toMaterialGroup: string;
    timeFunction: FunctionProvider;
    costFunction: FunctionProvider;
  }[]

  /** already fixed assignments, could be used for machine breakdowns or missing personell */
  fixedAssignments: {
    fromTime: number;
    toTime: number;
    /** defines if a started operation on this production resource can be split by this fixed assignment. It means: 0-100: Operation A starts, 101-200: fixed Assignment, 201-300: Operation A finishes */
    startedOperationsCanBeSplit: boolean;
  }[];

  currentSetupMaterialId: string;
}

export function isProductionResourceReqDto(v: unknown): v is ProductionResourceReqDto {
  return (
    typeof v === 'object' && v !== null &&
    'id' in v && typeof v.id === 'string' &&
    'producibleMaterials' in v && Array.isArray(v.producibleMaterials) && v.producibleMaterials.every(i => (
      typeof i === 'object' && i!== null &&
      'materialGroup' in i && typeof i.materialGroup ==='string' &&
      'timeFunction' in i && typeof isFunctionProvider(i.timeFunction) &&
      'costFunction' in i && typeof isFunctionProvider(i.costFunction)
    )) &&
    'bufferIdBefore' in v && typeof v.bufferIdBefore ==='string' &&
    'bufferIdAfter' in v && typeof v.bufferIdAfter ==='string' &&

    'changeovers' in v && Array.isArray(v.changeovers) && v.changeovers.every(i => (
      typeof i === 'object' && i!== null &&
      'fromMaterialGroup' in i && typeof i.fromMaterialGroup ==='string' &&
      'toMaterialGroup' in i && typeof i.toMaterialGroup ==='string' &&
      'timeFunction' in i && typeof isFunctionProvider(i.timeFunction) &&
      'costFunction' in i && typeof isFunctionProvider(i.costFunction)
    )) &&
    'fixedAssignments' in v && Array.isArray(v.fixedAssignments) && v.fixedAssignments.every(i => (
      typeof i === 'object' && i!== null &&
      'fromTime' in i && typeof i.fromTime === 'number' &&
      'toTime' in i && typeof i.toTime === 'number' &&
      'startedOperationsCanBeSplit' in i && typeof i.startedOperationsCanBeSplit === 'boolean'
    )) 
  )
}
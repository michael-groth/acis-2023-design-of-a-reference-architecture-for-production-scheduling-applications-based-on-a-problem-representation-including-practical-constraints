
export interface BufferReqDto {
  bufferId: string;
  size: number;
}

export function isBufferReqDto(bufferReqDto: unknown): bufferReqDto is BufferReqDto {
  return (
    typeof bufferReqDto === 'object' && bufferReqDto !== null &&
    'bufferId' in bufferReqDto && typeof bufferReqDto.bufferId === 'string' &&
    'size' in bufferReqDto && typeof bufferReqDto.size === 'number' 
  )
}

import { expect } from "chai";
import { describe, it } from "mocha";
import { OperationReqDto, isOperationReqDto } from "./operation.req-dto";

const exampleData: OperationReqDto = {
  operationId: 'operationId',
  amount: 1,
  producesMaterial: ['material1', 'material2'],
  madeFromMaterial: ['material1', 'material2'],
  predecessors: [
    {operationId: 'operationId1', successorHasNoWaitCondition: false},
  ],
};

describe('isOperationReqDto', () => {
  it('should work', () => {
    expect(isOperationReqDto(exampleData)).to.be.true;
  })
  it('should fail with missing values', () => {
    for (const k of Object.keys(exampleData)) {
      const tmp:any = {...exampleData};
      delete tmp[k]
      expect(isOperationReqDto(tmp)).to.be.false;
    }
  })
  it('should fail with wrong attribute values', () => {
    for (const k of Object.keys(exampleData)) {
      const tmp:any = {...exampleData};
      tmp[k] = null
      expect(isOperationReqDto(tmp)).to.be.false;
    }
  })
  it('should fail with wrong value as input', () => {
    expect(isOperationReqDto(123)).to.be.false
  })
  it('should work with optional initial buffer fill', () => {
    const tmp2: OperationReqDto = {...exampleData,
      initialBufferFill: {
        bufferId: '123',
        bufferUsage: 123,
      },
    }
    expect(isOperationReqDto(tmp2)).to.be.true
  })
})
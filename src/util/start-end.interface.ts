export interface StartEnd {
  start: number;
  end: number;
}
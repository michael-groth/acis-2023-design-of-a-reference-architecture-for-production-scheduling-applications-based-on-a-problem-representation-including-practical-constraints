
export class Random {

  public static randI(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
}

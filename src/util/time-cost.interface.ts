
export interface TimeCost {
  time: number;
  cost: number;
}

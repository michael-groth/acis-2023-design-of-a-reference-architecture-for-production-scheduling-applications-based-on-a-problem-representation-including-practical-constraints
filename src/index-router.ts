import 'reflect-metadata';
import Container from 'typedi';
import { App } from './app';

const app = Container.get(App)
app.init(process.env.port || 3000).catch(err => console.error('App could not be initialized', err));
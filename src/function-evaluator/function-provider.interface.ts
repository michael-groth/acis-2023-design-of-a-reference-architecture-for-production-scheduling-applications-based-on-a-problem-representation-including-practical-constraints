
export type FunctionProvider =
    number
    | FunctionProviderVariable
    | FunctionProviderAddition
    | FunctionProviderMultiplication

export function isFunctionProvider(provider:unknown): provider is FunctionProvider {
  return (
    typeof provider === 'number'
    || isFunctionProviderVariable(provider)
    || isFunctionProviderAddition(provider)
    || isFunctionProviderMultiplication(provider)
  )
}

export enum FunctionProviderVariable {
  /** number of time units until the last operation completes */
  TOTAL_MAXIMUM_MAKESPAN = 'totalMaximumMakespan',
  /** total costs to complete the schedule */
  TOTAL_COSTS = 'totalCosts',
  /** number 0-1 representing the relative amount of jobs completed in time */
  TIMELINESS = 'timeliness',
  /** sum of the time of all changeovers */
  TOTAL_CHANGEOVER_TIME = 'totalChangeoverTime',
  /** number used for time cost calculations */
  AMOUNT = 'amount',
}
export function isFunctionProviderVariable(provider: unknown): provider is FunctionProviderVariable {
  return (typeof provider === 'string' && Object.values(FunctionProviderVariable).includes(provider as FunctionProviderVariable))
}

export interface FunctionProviderAddition {
  type: 'add'
  value1: FunctionProvider
  value2: FunctionProvider
}
export function isFunctionProviderAddition(provider: unknown): provider is FunctionProviderAddition {
  return (
    typeof provider === 'object' && provider !== null &&
    'type' in provider && provider.type === 'add' &&
    'value1' in provider && isFunctionProvider(provider.value1) &&
    'value2' in provider && isFunctionProvider(provider.value2)
  )
}

export interface FunctionProviderMultiplication {
  type: 'mult'
  value1: FunctionProvider
  value2: FunctionProvider
}
export function isFunctionProviderMultiplication(provider: unknown): provider is FunctionProviderMultiplication {
  return (
    typeof provider === 'object' && provider !== null &&
    'type' in provider && provider.type === 'mult' &&
    'value1' in provider && isFunctionProvider(provider.value1) &&
    'value2' in provider && isFunctionProvider(provider.value2)
  )
}

export class FunctionProviderVariables {
  totalMaximumMakespan: number|null = null;
  totalCosts: number|null = null;
  timeliness: number|null = null
  totalChangeoverTime: number|null = null;
  amount: number|null = null;
}

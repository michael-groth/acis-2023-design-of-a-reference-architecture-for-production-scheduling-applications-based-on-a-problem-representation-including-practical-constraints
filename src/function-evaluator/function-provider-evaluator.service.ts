import { Service } from "typedi";
import { FunctionProviderVariables } from "./function-provider-variables.class";
import { FunctionProvider, FunctionProviderVariable, isFunctionProviderAddition, isFunctionProviderMultiplication, isFunctionProviderVariable } from "./function-provider.interface";

@Service()
export class FunctionProviderEvaluatorService {

  evaluateFunction(func: FunctionProvider, vars?: FunctionProviderVariables): number {
    // --- switch all types---
    // number
    if (typeof func === 'number') {
      return func;
    }
    // variable
    if (isFunctionProviderVariable(func)) {
      return this.getVariableValue(func, vars);
    }
    // addition
    if (isFunctionProviderAddition(func)) {
      return this.evaluateFunction(func.value1, vars) + this.evaluateFunction(func.value2, vars);
    }
    // multiplication
    if (isFunctionProviderMultiplication(func)) {
      return this.evaluateFunction(func.value1, vars) * this.evaluateFunction(func.value2, vars);
    }
    // not set
    throw new Error('Function type not implemented')
  }

  private getVariableValue(func: FunctionProviderVariable, vars?: FunctionProviderVariables): number {
    if (!vars) {
      throw new Error('vars not provided, variable could not be resolved')
    }
    if (vars[func] === null || vars[func] === undefined) {
      throw new Error(`Variable ${func} not defined in vars`);
    }
    return vars[func] as number;
  }


}

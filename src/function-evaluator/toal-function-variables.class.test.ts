import { expect } from "chai";
import { describe, it } from 'mocha';
import { GoalFunctionVariable } from './function-provider.interface';
import { GoalFunctionVariables } from './goal-function-variables.class';

describe('GoalFunctionVariables', () => {
  it('should contain all defined variables', () => {
    const vars = Object.values(GoalFunctionVariable);
    const testClass = new GoalFunctionVariables();
    for (const v of vars) {
      expect(testClass[v]).to.be.null;
    }
  })
})

import { expect } from "chai";
import { describe, it } from "mocha";
import 'reflect-metadata';
import Container from "typedi";
import { FunctionProviderEvaluatorService } from "./function-provider-evaluator.service";
import { FunctionProvider, GoalFunctionVariable } from "./function-provider.interface";
import { GoalFunctionVariables } from "./goal-function-variables.class";

describe('FunctionProviderEvaluatorService', () => {
  let service = Container.get(FunctionProviderEvaluatorService);
  describe('evaluateFunction', () => {
    it('should work with a number', () => {
      const input: FunctionProvider = 1;
      const result = service.evaluateFunction(input);
      expect(result).to.equal(1);
    })
    it('should work with a variable', () => {
      const input: FunctionProvider = GoalFunctionVariable.TIMELINESS;
      const vars = new GoalFunctionVariables();
      vars.timeliness = 0.5;
      const result = service.evaluateFunction(input, vars);
      expect(result).to.equal(0.5);
    })
    it('should work with an addition', () => {
      const input: FunctionProvider = {
        type: 'add',
        value1: 2,
        value2: 3,
      };
      const result = service.evaluateFunction(input);
      expect(result).to.equal(5);
    })
    it('should work with a multiplication', () => {
      const input: FunctionProvider = {
        type: 'mult',
        value1: 2,
        value2: 3,
      };
      const result = service.evaluateFunction(input);
      expect(result).to.equal(6);
    })
    it('should work with nested calculations', () => {
      const input: FunctionProvider = {
        type: 'mult',
        value1: 2,
        value2: {
          type: 'add',
          value1: 3,
          value2: 4,
        },
      };
      const result = service.evaluateFunction(input);
      expect(result).to.equal(14);
    })
  })
})
import { ProductionAidReqDto } from "../../request-data-transfer-objects/production-aid.req-dto";

export const aidsArchitectureDemonstration: ProductionAidReqDto[] = [
  {
    productionAidId: 'assembly-big',
    amountAvailable: 1,
  },
]
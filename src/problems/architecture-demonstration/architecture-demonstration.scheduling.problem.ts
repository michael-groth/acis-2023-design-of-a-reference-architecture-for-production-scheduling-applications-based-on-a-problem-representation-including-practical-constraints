import { FunctionProviderVariable } from "../../function-evaluator/function-provider.interface";
import { SchedulingProblemReqDto } from "../../request-data-transfer-objects/scheduling-problem.req-dto";
import { aidsArchitectureDemonstration } from "./aids";
import { buffersArchitectureDemonstration } from "./buffers";
import { jobsArchitectureDemonstration } from "./jobs";
import { materialsArchitectureDemonstration } from "./materials";
import { productionResourcesArchitectureDemonstration } from "./production-resources";
import { simultaneousConditionsArchitectureDemonstration } from "./simultaneous-conditions";

export const architectureDemonstrationSchedulingProblem: SchedulingProblemReqDto = {
  goalFunction: {
    type: 'mult',
    value1: -1,
    value2: FunctionProviderVariable.TOTAL_MAXIMUM_MAKESPAN,
  },

  jobs: jobsArchitectureDemonstration, 

  materials: materialsArchitectureDemonstration,

  simultaneousProductionConditions: simultaneousConditionsArchitectureDemonstration,

  productionResources: productionResourcesArchitectureDemonstration,

  productionAids: aidsArchitectureDemonstration,

  buffers: buffersArchitectureDemonstration,

}

import { FunctionProviderVariable } from "../../function-evaluator/function-provider.interface";
import { ProductionResourceReqDto } from "../../request-data-transfer-objects/production-resource.req-dto";

export const productionResourcesArchitectureDemonstration: ProductionResourceReqDto[] = [
  {
    id: 'frame-welding',
    producibleMaterials: [
      {
        materialGroup: 'frame-small',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 4,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
      {
        materialGroup: 'frame-big',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 5,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
    ],
    bufferIdBefore: 'start-end',
    bufferIdAfter: 'a',
    changeovers: [
      {
        fromMaterialGroup: 'frame-small',
        toMaterialGroup: 'frame-big',
        timeFunction: 30,
        costFunction: 0,
      },
      {
        fromMaterialGroup: 'frame-big',
        toMaterialGroup: 'frame-small',
        timeFunction: 30,
        costFunction: 0,
      },      
    ],
    fixedAssignments: [],
    currentSetupMaterialId: 'frame-small',
  },


  // assembly machines
  {
    id: 'assembly-a',
    producibleMaterials: [
      {
        materialGroup: 'assembled-small-a',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 8,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
      {
        materialGroup: 'assembled-small-b',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 9,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
      {
        materialGroup: 'assembled-big',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 11,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
    ],
    bufferIdBefore: 'a',
    bufferIdAfter: 'b',
    changeovers: [
      {
        fromMaterialGroup: 'assembled-small-a',
        toMaterialGroup: 'assembled-small-b',
        timeFunction: 50,
        costFunction: 0,
      },
      {
        fromMaterialGroup: 'assembled-small-a',
        toMaterialGroup: 'assembled-big',
        timeFunction: 80,
        costFunction: 0,
      },  
      {
        fromMaterialGroup: 'assembled-small-b',
        toMaterialGroup: 'assembled-small-a',
        timeFunction: 50,
        costFunction: 0,
      },
      {
        fromMaterialGroup: 'assembled-small-b',
        toMaterialGroup: 'assembled-big',
        timeFunction: 80,
        costFunction: 0,
      },     
      {
        fromMaterialGroup: 'assembled-big',
        toMaterialGroup: 'assembled-small-b',
        timeFunction: 80,
        costFunction: 0,
      },
      {
        fromMaterialGroup: 'assembled-big',
        toMaterialGroup: 'assembled-small-a',
        timeFunction: 80,
        costFunction: 0,
      },   
    ],
    fixedAssignments: [],
    currentSetupMaterialId: 'assembled-small-a',
  },
  {
    id: 'assembly-b',
    producibleMaterials: [
      {
        materialGroup: 'assembled-small-a',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 8,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
      {
        materialGroup: 'assembled-small-b',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 9,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
      {
        materialGroup: 'assembled-big',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 11,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
    ],
    bufferIdBefore: 'a',
    bufferIdAfter: 'b',
    changeovers: [
      {
        fromMaterialGroup: 'assembled-small-a',
        toMaterialGroup: 'assembled-small-b',
        timeFunction: 50,
        costFunction: 0,
      },
      {
        fromMaterialGroup: 'assembled-small-a',
        toMaterialGroup: 'assembled-big',
        timeFunction: 80,
        costFunction: 0,
      },  
      {
        fromMaterialGroup: 'assembled-small-b',
        toMaterialGroup: 'assembled-small-a',
        timeFunction: 50,
        costFunction: 0,
      },
      {
        fromMaterialGroup: 'assembled-small-b',
        toMaterialGroup: 'assembled-big',
        timeFunction: 80,
        costFunction: 0,
      },     
      {
        fromMaterialGroup: 'assembled-big',
        toMaterialGroup: 'assembled-small-b',
        timeFunction: 80,
        costFunction: 0,
      },
      {
        fromMaterialGroup: 'assembled-big',
        toMaterialGroup: 'assembled-small-a',
        timeFunction: 80,
        costFunction: 0,
      },   
    ],
    fixedAssignments: [],
    currentSetupMaterialId: 'assembled-small-b',
  },


  // lacquering
  {
    id: 'lacquering',
    producibleMaterials: [
      {
        materialGroup: 'lacquered-small-a',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 5,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
      {
        materialGroup: 'lacquered-small-b',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 7,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
      {
        materialGroup: 'lacquered-big',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 7,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
    ],
    bufferIdBefore: 'b',
    bufferIdAfter: 'b',
    changeovers: [
      {
        fromMaterialGroup: 'lacquered-small-a',
        toMaterialGroup: 'lacquered-small-b',
        timeFunction: 50,
        costFunction: 0,
      },
      {
        fromMaterialGroup: 'lacquered-small-a',
        toMaterialGroup: 'lacquered-big',
        timeFunction: 80,
        costFunction: 0,
      },  
      {
        fromMaterialGroup: 'lacquered-small-b',
        toMaterialGroup: 'lacquered-small-a',
        timeFunction: 50,
        costFunction: 0,
      },
      {
        fromMaterialGroup: 'lacquered-small-b',
        toMaterialGroup: 'lacquered-big',
        timeFunction: 80,
        costFunction: 0,
      },     
      {
        fromMaterialGroup: 'lacquered-big',
        toMaterialGroup: 'lacquered-small-b',
        timeFunction: 80,
        costFunction: 0,
      },
      {
        fromMaterialGroup: 'lacquered-big',
        toMaterialGroup: 'lacquered-small-a',
        timeFunction: 80,
        costFunction: 0,
      },   
    ],
    fixedAssignments: [],
    currentSetupMaterialId: 'lacquered-small-a',
  },

  
  // packaging
  {
    id: 'packaging-a',
    producibleMaterials: [
      {
        materialGroup: 'packaged-small-a',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 10,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
      {
        materialGroup: 'packaged-small-b',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 10,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
      {
        materialGroup: 'packaged-big',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 10,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
    ],
    bufferIdBefore: 'b',
    bufferIdAfter: 'start-end',
    changeovers: [
      {
        fromMaterialGroup: 'packaged-small-a',
        toMaterialGroup: 'packaged-small-b',
        timeFunction: 50,
        costFunction: 0,
      },
      {
        fromMaterialGroup: 'packaged-small-a',
        toMaterialGroup: 'packaged-big',
        timeFunction: 80,
        costFunction: 0,
      },  
      {
        fromMaterialGroup: 'packaged-small-b',
        toMaterialGroup: 'packaged-small-a',
        timeFunction: 50,
        costFunction: 0,
      },
      {
        fromMaterialGroup: 'packaged-small-b',
        toMaterialGroup: 'packaged-big',
        timeFunction: 80,
        costFunction: 0,
      },     
      {
        fromMaterialGroup: 'packaged-big',
        toMaterialGroup: 'packaged-small-b',
        timeFunction: 80,
        costFunction: 0,
      },
      {
        fromMaterialGroup: 'packaged-big',
        toMaterialGroup: 'packaged-small-a',
        timeFunction: 80,
        costFunction: 0,
      },   
    ],
    fixedAssignments: [],
    currentSetupMaterialId: 'packaged-small-a',
  },
  {
    id: 'packaging-b',
    producibleMaterials: [
      {
        materialGroup: 'packaged-small-a',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 10,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
      {
        materialGroup: 'packaged-small-b',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 10,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
      {
        materialGroup: 'packaged-big',
        timeFunction: {
          type: 'mult',
          value1: FunctionProviderVariable.AMOUNT,
          value2: 10,
        },
        costFunction: FunctionProviderVariable.AMOUNT,
      },
    ],
    bufferIdBefore: 'b',
    bufferIdAfter: 'start-end',
    changeovers: [
      {
        fromMaterialGroup: 'packaged-small-a',
        toMaterialGroup: 'packaged-small-b',
        timeFunction: 50,
        costFunction: 0,
      },
      {
        fromMaterialGroup: 'packaged-small-a',
        toMaterialGroup: 'packaged-big',
        timeFunction: 80,
        costFunction: 0,
      },  
      {
        fromMaterialGroup: 'packaged-small-b',
        toMaterialGroup: 'packaged-small-a',
        timeFunction: 50,
        costFunction: 0,
      },
      {
        fromMaterialGroup: 'packaged-small-b',
        toMaterialGroup: 'packaged-big',
        timeFunction: 80,
        costFunction: 0,
      },     
      {
        fromMaterialGroup: 'packaged-big',
        toMaterialGroup: 'packaged-small-b',
        timeFunction: 80,
        costFunction: 0,
      },
      {
        fromMaterialGroup: 'packaged-big',
        toMaterialGroup: 'packaged-small-a',
        timeFunction: 80,
        costFunction: 0,
      },   
    ],
    fixedAssignments: [],
    currentSetupMaterialId: 'packaged-small-a',
  },


]
import { ProductionJobReqDto } from "../../request-data-transfer-objects/production-job.req-dto";

export const jobsArchitectureDemonstration: ProductionJobReqDto[] = [
  {
    name: 'j1-small-a',
    priority: 1,
    dueDate: 1000,
    operations: [
      {
        operationId: 'j1-frame',
        amount: 10,
        producesMaterial: ['frame-small'],
        madeFromMaterial: 'start',
        predecessors: [],
      },
      {
        operationId: 'j1-assembly',
        amount: 10,
        producesMaterial: ['assembled-small-a'],
        madeFromMaterial: 'frame-small',
        predecessors: [
          {
            operationId: 'j1-frame',
            successorHasNoWaitCondition: false,
          },
        ],
      },
      {
        operationId: 'j1-packaging',
        amount: 10,
        producesMaterial: ['packaged-small-a'],
        madeFromMaterial: 'assembled-small-a',
        predecessors: [
          {
            operationId: 'j1-assembly',
            successorHasNoWaitCondition: false,
          },
        ],
      },
    ],
  },
  
  {
    name: 'j2-small-a-with-releaseDate',
    priority: 1,
    dueDate: 1100,
    releaseDate: 200,
    operations: [
      {
        operationId: 'j2-frame',
        amount: 10,
        producesMaterial: ['frame-small'],
        madeFromMaterial: 'start',
        predecessors: [],
      },
      {
        operationId: 'j2-assembly',
        amount: 10,
        producesMaterial: ['assembled-small-a'],
        madeFromMaterial: 'frame-small',
        predecessors: [
          {
            operationId: 'j2-frame',
            successorHasNoWaitCondition: false,
          },
        ],
      },
      {
        operationId: 'j2-packaging',
        amount: 10,
        producesMaterial: ['packaged-small-a'],
        madeFromMaterial: 'assembled-small-a',
        predecessors: [
          {
            operationId: 'j2-assembly',
            successorHasNoWaitCondition: false,
          },
        ],
      },
    ],
  },
  
  {
    name: 'j3-small-b-lacquered',
    priority: 1,
    dueDate: 1500,
    operations: [
      {
        operationId: 'j3-frame',
        amount: 20,
        producesMaterial: ['frame-small'],
        madeFromMaterial: 'start',
        predecessors: [],
      },
      {
        operationId: 'j3-assembly',
        amount: 20,
        producesMaterial: ['assembled-small-b'],
        madeFromMaterial: 'frame-small',
        predecessors: [
          {
            operationId: 'j3-frame',
            successorHasNoWaitCondition: false,
          },
        ],
      },
      {
        operationId: 'j3-lacquering',
        amount: 20,
        producesMaterial: ['lacquered-small-b'],
        madeFromMaterial: 'assembled-small-b',
        predecessors: [
          {
            operationId: 'j3-assembly',
            successorHasNoWaitCondition: false,
          },
        ],
      },
      {
        operationId: 'j3-packaging',
        amount: 20,
        producesMaterial: ['packaged-small-b'],
        madeFromMaterial: 'lacquered-small-b',
        predecessors: [
          {
            operationId: 'j3-lacquering',
            successorHasNoWaitCondition: false,
          },
        ],
      },
    ],
  },
  
  {
    name: 'j4-big-lacquered',
    priority: 1,
    dueDate: 1500,
    operations: [
      {
        operationId: 'j4-frame',
        amount: 5,
        producesMaterial: ['frame-big'],
        madeFromMaterial: 'start',
        predecessors: [],
      },
      {
        operationId: 'j4-assembly',
        amount: 5,
        producesMaterial: ['assembled-big'],
        madeFromMaterial: 'frame-big',
        predecessors: [
          {
            operationId: 'j4-frame',
            successorHasNoWaitCondition: false,
          },
        ],
        requiredProductionAids: [
          {
            productionAidId: 'assembly-big',
            amount: 1,
          }
        ]
      },
      {
        operationId: 'j4-lacquering',
        amount: 5,
        producesMaterial: ['lacquered-big'],
        madeFromMaterial: 'assembled-big',
        predecessors: [
          {
            operationId: 'j4-assembly',
            successorHasNoWaitCondition: false,
          },
        ],
      },
      {
        operationId: 'j4-packaging',
        amount: 5,
        producesMaterial: ['packaged-big'],
        madeFromMaterial: 'lacquered-big',
        predecessors: [
          {
            operationId: 'j4-lacquering',
            successorHasNoWaitCondition: false,
          },
        ],
      },
    ],
  },
  
  {
    name: 'j5-big',
    priority: 1,
    dueDate: 1500,
    operations: [
      {
        operationId: 'j5-frame',
        amount: 8,
        producesMaterial: ['frame-big'],
        madeFromMaterial: 'start',
        predecessors: [],
      },
      {
        operationId: 'j5-assembly',
        amount: 8,
        producesMaterial: ['assembled-big'],
        madeFromMaterial: 'frame-big',
        predecessors: [
          {
            operationId: 'j5-frame',
            successorHasNoWaitCondition: false,
          },
        ],
        requiredProductionAids: [
          {
            productionAidId: 'assembly-big',
            amount: 1,
          }
        ]
      },
      {
        operationId: 'j5-packaging',
        amount: 8,
        producesMaterial: ['packaged-big'],
        madeFromMaterial: 'assembled-big',
        predecessors: [
          {
            operationId: 'j5-assembly',
            successorHasNoWaitCondition: false,
          },
        ],
      },
    ],
  },





]
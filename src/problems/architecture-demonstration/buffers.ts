import { BufferReqDto } from "../../request-data-transfer-objects/buffer.req-dto";

export const buffersArchitectureDemonstration: BufferReqDto[] = [
  {
    bufferId: 'start-end',
    size: 999999999,
  },
  {
    bufferId: 'a',
    size: 25000,
  },
  {
    bufferId: 'b',
    size: 35000,
  },
]
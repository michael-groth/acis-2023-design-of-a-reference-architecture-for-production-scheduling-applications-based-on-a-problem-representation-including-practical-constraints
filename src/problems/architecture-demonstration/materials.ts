import { MaterialReqDto } from "../../request-data-transfer-objects/material.req-dto";

export const materialsArchitectureDemonstration: MaterialReqDto[] = [
  {
    materialId: 'start',
    name: 'start',
    bufferUsage: 0,
    materialGroups: ['start'],
  },
  {
    materialId: 'frame-small',
    name: 'frame-small',
    bufferUsage: 2,
    materialGroups: ['frame-small'],
  },
  {
    materialId: 'frame-big',
    name: 'frame-big',
    bufferUsage: 3,
    materialGroups: ['frame-big'],
  },
  {
    materialId: 'assembled-small-a',
    name: 'assembled-small-a',
    bufferUsage: 2,
    materialGroups: ['assembled-small-a'],
  },
  {
    materialId: 'assembled-small-b',
    name: 'assembled-small-b',
    bufferUsage: 2,
    materialGroups: ['assembled-small-b'],
  },
  {
    materialId: 'assembled-big',
    name: 'assembled-big',
    bufferUsage: 3,
    materialGroups: ['assembled-big'],
  },
  {
    materialId: 'lacquered-small-a',
    name: 'lacquered-small-a',
    bufferUsage: 2,
    materialGroups: ['lacquered-small-a'],
  },
  {
    materialId: 'lacquered-small-b',
    name: 'lacquered-small-b',
    bufferUsage: 2,
    materialGroups: ['lacquered-small-b'],
  },
  {
    materialId: 'lacquered-big',
    name: 'lacquered-big',
    bufferUsage: 3,
    materialGroups: ['lacquered-big'],
  },
  {
    materialId: 'packaged-small-a',
    name: 'packaged-small-a',
    bufferUsage: 2,
    materialGroups: ['packaged-small-a'],
  },
  {
    materialId: 'packaged-small-b',
    name: 'packaged-small-b',
    bufferUsage: 2,
    materialGroups: ['packaged-small-b'],
  },
  {
    materialId: 'packaged-big',
    name: 'packaged-big',
    bufferUsage: 3,
    materialGroups: ['packaged-big'],
  },
]
import { SimultaneousProductionConditionReqDto, SimultaneousProductionConditionType } from "../../request-data-transfer-objects/simultaneous-production-condition.req-dto";

export const simultaneousConditionsArchitectureDemonstration: SimultaneousProductionConditionReqDto[] = [
  {
    productionResourceIds: ['packaging-a', 'packaging-b'],
    type: SimultaneousProductionConditionType.MATERIAL_EXCLUSIONS,
    value1: 'packaged-small-b',
    value2: 'packaged-big',
  },
]
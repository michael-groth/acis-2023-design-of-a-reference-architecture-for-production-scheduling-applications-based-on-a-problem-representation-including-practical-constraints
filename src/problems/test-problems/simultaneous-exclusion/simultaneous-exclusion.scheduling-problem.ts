import { FunctionProviderVariable } from "../../function-evaluator/function-provider.interface";
import { SchedulingProblemReqDto } from "../../request-data-transfer-objects/scheduling-problem.req-dto";
import { SimultaneousProductionConditionType } from "../../request-data-transfer-objects/simultaneous-production-condition.req-dto";

export const simultaneousExclusionSchedulingProblem: SchedulingProblemReqDto = {
  goalFunction: {
    type: 'mult',
    value1: -1,
    value2: FunctionProviderVariable.TOTAL_MAXIMUM_MAKESPAN,
  },

  jobs: [
    {
      name: '1-a',
      priority: 1,
      dueDate: 100,
      operations: [
        {
          operationId: '1-a-o-1',
          amount: 10,
          producesMaterial: ['a'],
          madeFromMaterial: 'b',
          predecessors: [
            {
              operationId: '1-a-o-2',
              successorHasNoWaitCondition: false,
            }
          ],
        },
         {
          operationId: '1-a-o-2',
          amount: 10,
          producesMaterial: ['b'],
          madeFromMaterial: 'start',
          predecessors: [],
         }
      ],
    },
    {
      name: '2-b',
      priority: 1,
      dueDate: 100,
      operations: [
        {
          operationId: '2-b-o-1',
          amount: 10,
          producesMaterial: ['b'],
          madeFromMaterial: 'a',
          predecessors: [
            {
              operationId: '2-b-o-2',
              successorHasNoWaitCondition: false,
            }
          ],
        },
        {
          operationId: '2-b-o-2',
          amount: 2,
          producesMaterial: ['a'],
          madeFromMaterial: 'start',
          predecessors: [],
        },
      ],
    },
    {
      name: '3-multi-many',
      priority: 1,
      dueDate: 100,
      operations: [
        {
          operationId: '3-1',
          amount: 3,
          producesMaterial: ['b'],
          madeFromMaterial: 'start',
          predecessors: [],
        },
        {
          operationId: '3-2',
          amount: 3,
          producesMaterial: ['a'],
          madeFromMaterial: 'start',
          predecessors: [],
        },
        {
          operationId: '3-3',
          amount: 3,
          producesMaterial: ['b'],
          madeFromMaterial: 'start',
          predecessors: [],
        },
        {
          operationId: '3-4',
          amount: 3,
          producesMaterial: ['a'],
          madeFromMaterial: 'start',
          predecessors: [],
        },
        {
          operationId: '3-5',
          amount: 3,
          producesMaterial: ['b'],
          madeFromMaterial: 'start',
          predecessors: [],
        },
      ],
    },
  ], 

  materials: [
    {
      materialId: 'start',
      name: 'start',
      bufferUsage: 0,
      materialGroups: ['start'],
    },
    {
      materialId: 'a',
      name: 'a',
      bufferUsage: 0,
      materialGroups: ['a', '1-without-b', '1-without-c'],
    },
    {
      materialId: 'b',
      name: 'b',
      bufferUsage: 0,
      materialGroups: ['b', '1-without-a', '1-without-c'],
    },
  ],

  simultaneousProductionConditions: [
    {
      type: SimultaneousProductionConditionType.MATERIAL_EXCLUSIONS,
      value1: 'a',
      value2: 'b',
      productionResourceIds: ['main1-a', 'main2-b']
    }
  ],

  productionResources: [
    {
      id: 'main1-a',
      producibleMaterials: [
        {
          materialGroup: 'a',
          timeFunction: {
            type: 'mult',
            value1: FunctionProviderVariable.AMOUNT,
            value2: 5,
          },
          costFunction: FunctionProviderVariable.AMOUNT,
        },
        {
          materialGroup: 'b',
          timeFunction: {
            type: 'mult',
            value1: FunctionProviderVariable.AMOUNT,
            value2: 3,
          },
          costFunction: FunctionProviderVariable.AMOUNT,
        },
      ],
      bufferIdBefore: 'big',
      bufferIdAfter: 'big',
      changeovers: [ ],
      fixedAssignments: [],
      currentSetupMaterialId: 'a',
    },
    {
      id: 'main2-b',
      producibleMaterials: [
        {
          materialGroup: 'a',
          timeFunction: {
            type: 'mult',
            value1: FunctionProviderVariable.AMOUNT,
            value2: 5,
          },
          costFunction: FunctionProviderVariable.AMOUNT,
        },
        {
          materialGroup: 'b',
          timeFunction: {
            type: 'mult',
            value1: FunctionProviderVariable.AMOUNT,
            value2: 3,
          },
          costFunction: FunctionProviderVariable.AMOUNT,
        },
      ],
      bufferIdBefore: 'big',
      bufferIdAfter: 'big',
      changeovers: [ ],
      fixedAssignments: [],
      currentSetupMaterialId: 'b',
    },
  ],

  productionAids: [],

  buffers: [
    {
      bufferId: 'big',
      size: 1000000,
    },
  ],

}

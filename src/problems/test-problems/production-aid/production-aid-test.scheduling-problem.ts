import { FunctionProviderVariable } from "../../../function-evaluator/function-provider.interface";
import { SchedulingProblemReqDto } from "../../../request-data-transfer-objects/scheduling-problem.req-dto";

export const productionAidTestSchedulingProblem: SchedulingProblemReqDto = {
  goalFunction: {
    type: 'mult',
    value1: -1,
    value2: FunctionProviderVariable.TOTAL_MAXIMUM_MAKESPAN,
  },

  jobs: [
    {
      name: '1-a',
      priority: 1,
      dueDate: 100,
      operations: [
        {
          operationId: '1-a-o-1',
          amount: 10,
          producesMaterial: ['a'],
          madeFromMaterial: 'start',
          predecessors: [],
          minimumStoragePeriod: 0,
          requiredProductionAids: [
            {
              productionAidId: 'aid1',
              amount: 2,
            }
          ],
        },
        {
          operationId: '1-a-o-2',
          amount: 10,
          producesMaterial: ['a'],
          madeFromMaterial: 'start',
          predecessors: [],
          minimumStoragePeriod: 0,
          requiredProductionAids: [
            {
              productionAidId: 'aid1',
              amount: 2,
            }
          ],
        },
        {
          operationId: '1-a-o-3',
          amount: 10,
          producesMaterial: ['a'],
          madeFromMaterial: 'start',
          predecessors: [],
          minimumStoragePeriod: 0,
          requiredProductionAids: [
            {
              productionAidId: 'aid1',
              amount: 2,
            }
          ],
        },
        {
          operationId: '1-a-o-4',
          amount: 10,
          producesMaterial: ['a'],
          madeFromMaterial: 'start',
          predecessors: [],
          minimumStoragePeriod: 0,
          requiredProductionAids: [
            {
              productionAidId: 'aid1',
              amount: 2,
            }
          ],
        },
        {
          operationId: '1-a-o-5',
          amount: 10,
          producesMaterial: ['a'],
          madeFromMaterial: 'start',
          predecessors: [],
          minimumStoragePeriod: 0,
          requiredProductionAids: [
            {
              productionAidId: 'aid1',
              amount: 2,
            }
          ],
        },
      ],
    },
  ], 

  materials: [
    {
      materialId: 'start',
      name: 'start',
      bufferUsage: 0,
      materialGroups: ['start'],
    },
    {
      materialId: 'a',
      name: 'a',
      bufferUsage: 0,
      materialGroups: ['a', '1-without-b', '1-without-c'],
    },
  ],

  simultaneousProductionConditions: [],

  productionResources: [
    {
      id: 'main1-a',
      producibleMaterials: [
        {
          materialGroup: 'a',
          timeFunction: {
            type: 'mult',
            value1: FunctionProviderVariable.AMOUNT,
            value2: 5,
          },
          costFunction: FunctionProviderVariable.AMOUNT,
        },
      ],
      bufferIdBefore: 'big',
      bufferIdAfter: 'big',
      changeovers: [ ],
      fixedAssignments: [],
      currentSetupMaterialId: 'a',
    },
    {
      id: 'main2-a',
      producibleMaterials: [
        {
          materialGroup: 'a',
          timeFunction: {
            type: 'mult',
            value1: FunctionProviderVariable.AMOUNT,
            value2: 5,
          },
          costFunction: FunctionProviderVariable.AMOUNT,
        },
      ],
      bufferIdBefore: 'big',
      bufferIdAfter: 'big',
      changeovers: [ ],
      fixedAssignments: [],
      currentSetupMaterialId: 'a',
    },
  ],

  productionAids: [
    {
      productionAidId: 'aid1',
      amountAvailable: 3,
    },
  ],

  buffers: [
    {
      bufferId: 'big',
      size: 1000000,
    },
  ],

}

import { FunctionProviderVariable } from "../../../function-evaluator/function-provider.interface";
import { SchedulingProblemReqDto } from "../../../request-data-transfer-objects/scheduling-problem.req-dto";

export const releaseDateSchedulingProblem: SchedulingProblemReqDto = {
  goalFunction: {
    type: 'mult',
    value1: -1,
    value2: FunctionProviderVariable.TOTAL_MAXIMUM_MAKESPAN,
  },

  jobs: [
    {
      name: '1-a',
      priority: 1,
      dueDate: 100,
      releaseDate: 20,
      operations: [
        {
          operationId: '1-a-o-1',
          amount: 10,
          producesMaterial: ['a'],
          madeFromMaterial: 'start',
          predecessors: [],
        }
      ],
    },
    {
      name: '2-b',
      priority: 1,
      dueDate: 100,
      operations: [
        {
          operationId: '2-b-o-1',
          amount: 10,
          producesMaterial: ['b'],
          madeFromMaterial: 'start',
          predecessors: [],
        }
      ],
    },
    {
      name: '3-c',
      priority: 1,
      dueDate: 100,
      operations: [
        {
          operationId: '3-c-o-1',
          amount: 10,
          producesMaterial: ['c'],
          madeFromMaterial: 'a',
          predecessors: [
            {
              operationId: '3-c-o-2',
              successorHasNoWaitCondition: false,
            }
          ],
        },
        {
          operationId: '3-c-o-2',
          amount: 2,
          producesMaterial: ['a'],
          madeFromMaterial: 'start',
          predecessors: [],
        },
      ],
    },
  ], 

  materials: [
    {
      materialId: 'start',
      name: 'start',
      bufferUsage: 0,
      materialGroups: ['start'],
    },
    {
      materialId: 'a',
      name: 'a',
      bufferUsage: 0,
      materialGroups: ['a', '1-without-b', '1-without-c'],
    },
    {
      materialId: 'b',
      name: 'b',
      bufferUsage: 0,
      materialGroups: ['b', '1-without-a', '1-without-c'],
    },
    {
      materialId: 'c',
      name: 'c',
      bufferUsage: 0,
      materialGroups: ['c', '1-without-a', '1-without-b'],
    },
  ],

  simultaneousProductionConditions: [],

  productionResources: [
    {
      id: 'main',
      producibleMaterials: [
        {
          materialGroup: 'a',
          timeFunction: {
            type: 'mult',
            value1: FunctionProviderVariable.AMOUNT,
            value2: 5,
          },
          costFunction: FunctionProviderVariable.AMOUNT,
        },
        {
          materialGroup: 'b',
          timeFunction: {
            type: 'mult',
            value1: FunctionProviderVariable.AMOUNT,
            value2: 3,
          },
          costFunction: FunctionProviderVariable.AMOUNT,
        },
        {
          materialGroup: 'c',
          timeFunction: {
            type: 'mult',
            value1: FunctionProviderVariable.AMOUNT,
            value2: 2,
          },
          costFunction: FunctionProviderVariable.AMOUNT,
        },
      ],
      bufferIdBefore: 'big',
      bufferIdAfter: 'big',
      changeovers: [
        {
          fromMaterialGroup: 'a',
          toMaterialGroup: '1-without-a',
          timeFunction: 30,
          costFunction: 0,
        },
        {
          fromMaterialGroup: 'b',
          toMaterialGroup: '1-without-b',
          timeFunction: 30,
          costFunction: 0,
        },
        {
          fromMaterialGroup: 'c',
          toMaterialGroup: '1-without-c',
          timeFunction: 30,
          costFunction: 0,
        },
        
      ],
      fixedAssignments: [],
      currentSetupMaterialId: 'a',
    }
  ],

  productionAids: [],

  buffers: [
    {
      bufferId: 'big',
      size: 1000000,
    },
  ],

}

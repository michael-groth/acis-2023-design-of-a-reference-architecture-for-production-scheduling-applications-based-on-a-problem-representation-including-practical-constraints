import { FunctionProviderVariable } from "../../../function-evaluator/function-provider.interface";
import { SchedulingProblemReqDto } from "../../../request-data-transfer-objects/scheduling-problem.req-dto";

export const minimumStoragePeriodTestSchedulingProblem: SchedulingProblemReqDto = {
  goalFunction: {
    type: 'mult',
    value1: -1,
    value2: FunctionProviderVariable.TOTAL_MAXIMUM_MAKESPAN,
  },

  jobs: [
    {
      name: '1-a',
      priority: 1,
      dueDate: 100,
      operations: [
        {
          operationId: '1-a-o-1',
          amount: 10,
          producesMaterial: ['a'],
          madeFromMaterial: 'b',
          predecessors: [
            {
              operationId: '1-a-o-2',
              successorHasNoWaitCondition: false,
            }
          ],
          minimumStoragePeriod: 44,
        },
         {
          operationId: '1-a-o-2',
          amount: 10,
          producesMaterial: ['b'],
          madeFromMaterial: 'start',
          predecessors: [],
          minimumStoragePeriod: 3300,
         }
      ],
    },
  ], 

  materials: [
    {
      materialId: 'start',
      name: 'start',
      bufferUsage: 0,
      materialGroups: ['start'],
    },
    {
      materialId: 'a',
      name: 'a',
      bufferUsage: 0,
      materialGroups: ['a', '1-without-b', '1-without-c'],
    },
    {
      materialId: 'b',
      name: 'b',
      bufferUsage: 0,
      materialGroups: ['b', '1-without-a', '1-without-c'],
    },
  ],

  simultaneousProductionConditions: [],

  productionResources: [
    {
      id: 'main1-a',
      producibleMaterials: [
        {
          materialGroup: 'a',
          timeFunction: {
            type: 'mult',
            value1: FunctionProviderVariable.AMOUNT,
            value2: 5,
          },
          costFunction: FunctionProviderVariable.AMOUNT,
        },
        {
          materialGroup: 'b',
          timeFunction: {
            type: 'mult',
            value1: FunctionProviderVariable.AMOUNT,
            value2: 3,
          },
          costFunction: FunctionProviderVariable.AMOUNT,
        },
      ],
      bufferIdBefore: 'big',
      bufferIdAfter: 'big',
      changeovers: [ ],
      fixedAssignments: [],
      currentSetupMaterialId: 'a',
    },
  ],

  productionAids: [],

  buffers: [
    {
      bufferId: 'big',
      size: 1000000,
    },
  ],

}

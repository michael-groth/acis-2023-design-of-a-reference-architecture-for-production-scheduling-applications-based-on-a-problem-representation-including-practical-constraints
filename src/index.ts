import * as fs from 'fs';
import 'reflect-metadata';
import Container from 'typedi';
import { architectureDemonstrationSchedulingProblem } from './problems/architecture-demonstration/architecture-demonstration.scheduling.problem';
import { FifoSchedulerService } from './schedulers/fifo/fifo-scheduler.service';
import { RlSelfPlaySchedulerService } from './schedulers/rl-self-play/rl-self-play-scheduler.service';


const rlSelfPlaySchedulerScherive = Container.get(RlSelfPlaySchedulerService);
const fifoSchedulerService = Container.get(FifoSchedulerService);
console.log('started')
const res = fifoSchedulerService.generateScheduling(architectureDemonstrationSchedulingProblem).then((res) => {
  console.log('done')
  console.log(res);
  fs.writeFileSync('output.json', JSON.stringify(res, null, 2));
});
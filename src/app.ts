import express from "express";
import { Service } from "typedi";
import { routes } from "./routes";


@Service()
export class App {

  private app: express.Application

  constructor() {
    this.app = express()
    this.initializeMiddleware();
    this.registerRoutes();
  }

  async init(port: number|string): Promise<void> {
    this.app.listen(port);
    console.log(`App listening on ${port}`)
  }


  private initializeMiddleware() {
    // todo - bisher nichts
  }

  private registerRoutes() {
    this.app.use(routes);
  }


}
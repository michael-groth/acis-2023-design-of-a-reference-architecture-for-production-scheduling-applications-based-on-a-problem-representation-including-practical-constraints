import Container, { Service } from "typedi";
import { SchedulingProblemReqDto } from "../../request-data-transfer-objects/scheduling-problem.req-dto";
import { ScheduleResDto } from "../../response-data-transfer-objects/schedule.res-dto";
import { SchedulingProblem } from "../content-classes/scheduling-problem.class";
import { SchedulingProblemFactory } from "../content-classes/scheduling-problem.factory";
import { SchedulerService } from "../interfaces/scheduler.service.interface";
import { OperationResourceAlternative } from "../rl-self-play/model-wrapper.class";

@Service()
export class FifoSchedulerService implements SchedulerService {
  async generateScheduling(schedulingProblem: SchedulingProblemReqDto): Promise<ScheduleResDto> {
    // initialize, create problem
    const problem = Container.get(SchedulingProblemFactory).generateSchedulingProblem(schedulingProblem);
    // loop over all open operations
    let done = false;
    while (!done) {
      done = this.scheduleStep(problem)
    }
    // calculate goal variables
    problem.calculateGoalValue();
    // return schedule
    return problem.convertToRes();
  }

  updateTrainingData?(schedulingProblem: SchedulingProblemReqDto): void {
    // method not required
    return;
  }

  scheduleStep(schedulingProblem: SchedulingProblem): boolean {
    // load open operations
    const openOperations = schedulingProblem.getAllProducibleOperations();
    // check if done
    if (openOperations.length === 0) return true;
    // build decision array, combine operations with production resources
    const alternatives: OperationResourceAlternative[] = [];
    for (const o of openOperations) {
      for (const pr of schedulingProblem.productionResources) {
        if (pr.canProduceOperation(o) && pr.isBufferAfterAvailableToProduce(o)) {
          alternatives.push({
            operation: o,
            productionResource: pr,
          })
        }
      }
    }
    //console.log('alternatives:')
    //console.log(alternatives)
    // decide
    if (alternatives.length === 0) {
      throw new Error('No alternatives for decision available.');
    }
    const decision = this.decideOnOperation(alternatives);
    //console.log(`decided for ${decision.operation.id} on ${decision.productionResource.id}`)
    // decision consequences
    this.planDecisionIntoProblem(schedulingProblem, decision);
    // return false, because probably not done yet
    return false;
  }

  decideOnOperation(alternatives: OperationResourceAlternative[]): OperationResourceAlternative {
    // init
    let currentLowestTime: number|undefined;
    let currentResourceWaitTime: number|undefined;
    let currentLowestResourceTime: number|undefined;
    let currentBestAlternative: OperationResourceAlternative|undefined;
    // loop
    for (const a of alternatives) {
      const startTime = a.productionResource.getStartTimeOf(a.operation, true);
      const waitTime = startTime - a.operation.availableAt;
      if (currentLowestTime === undefined || currentLowestTime > a.operation.availableAt) {
        currentLowestTime = a.operation.availableAt;
        currentBestAlternative = a;
        currentResourceWaitTime = waitTime
        currentLowestResourceTime = a.operation.availableAt;
      } else if (currentLowestTime === a.operation.availableAt && waitTime < currentResourceWaitTime) { // same time, but lower resource wait time
        currentLowestTime = a.operation.availableAt;
        currentBestAlternative = a;
        currentResourceWaitTime = waitTime
        currentLowestResourceTime = a.operation.availableAt;
      } else if (currentLowestTime === a.operation.availableAt && waitTime === currentResourceWaitTime && a.productionResource.nextAvailable < currentLowestResourceTime) { // same time and resource wait time, but lower resource time
        currentLowestTime = a.operation.availableAt;
        currentBestAlternative = a;
        currentResourceWaitTime = waitTime
        currentLowestResourceTime = a.operation.availableAt;
      }
    }
    // return
    if (!currentBestAlternative) throw new Error('Fifo Scheduler decode on operation: no best alternative found')
    return currentBestAlternative;
  }

  private planDecisionIntoProblem(schedulingProblem: SchedulingProblem, decision: OperationResourceAlternative): void {
    // add production resource assignment
    const operationStartEnd = decision.productionResource.addAssignment(decision.operation);
    //console.log(`start end: ${operationStartEnd.start} ${operationStartEnd.end}`)
    // reduce predecessor list in jobs operations and update timings
    decision.operation.processPlannedOperation(operationStartEnd);
  }

}
import { Service } from "typedi";
import { ProductionJobReqDto } from "../../request-data-transfer-objects/production-job.req-dto";
import { SchedulingProblemReqDto } from "../../request-data-transfer-objects/scheduling-problem.req-dto";
import { ProductionBuffer } from "./buffer.class";
import { Job } from "./job.class";
import { Material } from "./material.class";
import { Operation, OperationPredecessor, RequiredProductionAid } from "./operation.class";
import { ProductionAid } from "./production-aid.class";
import { ProductionResource } from "./production-resource.class";
import { SchedulingProblem } from "./scheduling-problem.class";
import { SimultaneousProductionCondition } from "./simultaneous-production-condition.class";

@Service()
export class SchedulingProblemFactory {

  generateSchedulingProblem(req: SchedulingProblemReqDto): SchedulingProblem {
    // prepare buffers
    const buffers = this.getProductionBuffers(req);
    // prepare productionAids
    const aids = this.getProductionAids(req);
    // prepare materials
    const materials = this.getMaterials(req);
    // prepare simultaneous production conditions
    const simultaneousProductionConditions = this.getSimultaneousProductionConditions(req);
    // prepare jobs
    const jobs = this.getJobs(req, materials, aids);
    // prepare productionResources
    const productionResources = this.getProductionResources(req, buffers, materials, simultaneousProductionConditions);
    // prepare problem
    const problem = new SchedulingProblem(req.goalFunction, jobs, materials, simultaneousProductionConditions, productionResources, aids, buffers);
    // return
    return problem;
  }

  private getSimultaneousProductionConditions(req: SchedulingProblemReqDto): SimultaneousProductionCondition[] {
    const tmp: SimultaneousProductionCondition[] = [];
    for (const c of req.simultaneousProductionConditions) {
      tmp.push(new SimultaneousProductionCondition(c.productionResourceIds, c.type, c.value1, c.value2));
    }
    return tmp;
  }

  private getProductionBuffers(req: SchedulingProblemReqDto): ProductionBuffer[] {
    const tmp: ProductionBuffer[] = [];
    for (const b of req.buffers) {
      tmp.push(new ProductionBuffer(b.bufferId, b.size))
    }
    return tmp;
  }

  private getProductionAids(req: SchedulingProblemReqDto): ProductionAid[] {
    const tmp: ProductionAid[] = [];
    for (const a of req.productionAids) {
      tmp.push(new ProductionAid(a.productionAidId, a.amountAvailable))
    }
    return tmp;
  }

  private getMaterials(req: SchedulingProblemReqDto): Material[] {
    const tmp: Material[] = [];
    for (const m of req.materials) {
      tmp.push(new Material(m.materialId, m.name, m.bufferUsage, m.materialGroups))
    }
    return tmp;
  }

  private getJobs(req: SchedulingProblemReqDto, materials: Material[], productionAids: ProductionAid[]): Job[] {
    const tmp: Job[] = [];
    for (const j of req.jobs) {
      const releaseDate = j.releaseDate | 0;
      const operations = this.getOperations(j, materials, productionAids, releaseDate);
      tmp.push(new Job(j.name, j.dueDate, j.priority, operations));
    }
    return tmp;
  }

  private getOperations(reqJob:ProductionJobReqDto, materials: Material[], productionAids: ProductionAid[], releaseDate: number): Operation[] {
    // copy all operations
    const reqOperations = [...reqJob.operations];
    const operations: Operation[] = [];
    // create list of final operations
    while(reqOperations.length > 0) {
      forOperations: for (const r of reqOperations) {
        // check if all predecessors are already in array
        const predecessors: OperationPredecessor[] = [];
        findPredecessors: for (const p of r.predecessors) {
          for (const ipr of operations) {
            if (ipr.id === p.operationId) {
              predecessors.push({
                operation: ipr,
                successorHasNoWaitCondition: p.successorHasNoWaitCondition,
              });
              continue findPredecessors;
            }
          }
          // not found, continue with next operation
          continue forOperations;
        }
        // all predecessors found, transform operation
        // build materials
        const producesMaterials: Material[] = [];
        pm: for (const pm of r.producesMaterial) {
          for (const m of materials) {
            if (m.id === pm) {
              producesMaterials.push(m);
              continue pm;
            }
          }
        }
        if (producesMaterials.length !== r.producesMaterial.length) {
          throw new Error('Not all Materials found');
        }
        // made from material
        let madeFromMaterial: Material|undefined;
        for (const m of materials) {
          if (m.id === r.madeFromMaterial) {
            madeFromMaterial = m;
            break;
          }
        }
        if (!madeFromMaterial) throw new Error(`Material ${r.madeFromMaterial} not found`);
        // required production aids
        const requiredProductionAids: RequiredProductionAid[] = [];
        if (r.requiredProductionAids) {
          for (const pa of r.requiredProductionAids) {
            let productionAid: ProductionAid|undefined;
            for (const aid of productionAids) {
              if (aid.id === pa.productionAidId) {
                productionAid = aid;
                break;
              }
            }
            if (!productionAid) throw new Error('Production Aid not found');
            requiredProductionAids.push({ productionAid, amount: pa.amount});
          }
        }
        // build operation
        operations.push(new Operation(r.operationId, r.amount, producesMaterials, madeFromMaterial, predecessors, r.minimumStoragePeriod || 0, requiredProductionAids, releaseDate ))
        // remove operation from open list
        reqOperations.splice(reqOperations.indexOf(r), 1)
        break forOperations;
      }
    }
    // return
    return operations;
  }

  private getProductionResources(req: SchedulingProblemReqDto, buffers: ProductionBuffer[], materials: Material[], simultaneousProductionConditions: SimultaneousProductionCondition[]): ProductionResource[] {
    // prepare
    const tmp: ProductionResource[] = [];
    // loop
    for (const pr of req.productionResources) {
      // find buffer before
      let bufferBefore: ProductionBuffer|undefined;
      for (const b of buffers) {
        if (b.id === pr.bufferIdBefore) {
          bufferBefore = b;
          break;
        }
      }
      if (!bufferBefore) throw new Error(`Buffer ${pr.bufferIdBefore} not found`);
      // find buffer after
      let bufferAfter: ProductionBuffer|undefined;
      for (const b of buffers) {
        if (b.id === pr.bufferIdAfter) {
          bufferAfter = b;
          break;
        }
      }
      if (!bufferAfter) throw new Error(`Buffer ${pr.bufferIdAfter} not found`);
      // currentSetupMaterial
      let currentSetupMaterial: Material|undefined;
      for (const m of materials) {
        if (m.id === pr.currentSetupMaterialId) {
          currentSetupMaterial = m;
          break;
        }
      }
      if (!currentSetupMaterial) throw new Error('Material not found')
      // simultaneous production conditions
      const cons: SimultaneousProductionCondition[] = [];
      for (const c of simultaneousProductionConditions) {
        if (c.productionResourceIds.includes(pr.id)) {
          cons.push(c);
        }
      }
      // build
      tmp.push(new ProductionResource(
        pr.id,
        pr.producibleMaterials,
        bufferBefore,
        bufferAfter,
        pr.changeovers,
        pr.fixedAssignments,
        currentSetupMaterial,
        cons,
      ))
    }
    // return
    return tmp;
  }

}
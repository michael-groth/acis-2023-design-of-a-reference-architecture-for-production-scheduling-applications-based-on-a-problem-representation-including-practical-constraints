import { SimultaneousProductionConditionType } from "../../request-data-transfer-objects/simultaneous-production-condition.req-dto";

export class SimultaneousProductionCondition {
  constructor(
    public productionResourceIds: string[],
    public type: SimultaneousProductionConditionType,
    public value1?:string,
    public value2?: string,
  ) {}
}

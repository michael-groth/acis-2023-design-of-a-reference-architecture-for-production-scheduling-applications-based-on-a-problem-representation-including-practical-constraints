
export class Material {
  constructor(
    public id: string,
    public name: string,
    public bufferUsage: number,
    public materialGroups: string[],
  ) {}

  isInMaterialGroup(group: string): boolean {
    return this.materialGroups.includes(group);
  }
}

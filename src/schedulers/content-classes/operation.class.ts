import { StartEnd } from "../../util/start-end.interface";
import { Job } from "./job.class";
import { Material } from "./material.class";
import { ProductionAid } from "./production-aid.class";
import { SchedulingProblem } from "./scheduling-problem.class";

export class Operation {

  scheduled = false;
  toSchedulePredecessors: OperationPredecessor[];
  //availableAt: number = -1 // must be updated when it gets available
  job: Job|undefined; // Must be updated after creation

  constructor(
    public id: string,
    public amount: number,
    public producesMaterial: Material[],
    public madeFromMaterial: Material,
    public predecessors: OperationPredecessor[],
    public minimumStoragePeriod: number,
    public requiredProductionAids: RequiredProductionAid[],
    public availableAt: number,
  ) {
    this.toSchedulePredecessors = predecessors;
    /*if (this.toSchedulePredecessors.length === 0) {
      this.availableAt = 0; // available at the start
    }*/
  }

  getRemainingTimeUntilJobDueDateAfterThisCompletition(now: number, timeThisOperation: number): number {
    if (this.job === undefined) throw new Error('Operation: job not set')
    return this.job.dueDate - (now + timeThisOperation);
  }

  getJobMinimumRequiredRemainingProductionTimeAfterCompletitionOfThisOperation(now: number, timeThisOperation: number): number {
    if (this.job === undefined) throw new Error('Operation: job not set')
    return this.job.getMinimumRequiredRemainingProductionTime(this);
  }

  getMinimumRequiredRemainingProductionTimeAfterThisAndPreviousOperations(schedulingProblem: SchedulingProblem, excludedOperation: Operation): number {
    // this is exluded operation
    if (this === excludedOperation) return 0;
    // find minimum own time
    let minimumOwnTime: undefined|number;
    for (const pr of schedulingProblem.productionResources) {
      if (pr.canProduceOperation(this)) {
        const timeCost = pr.getOperationTimeCost(this);
        if (!minimumOwnTime || timeCost.time < minimumOwnTime) {
          minimumOwnTime = timeCost.time;
        }
      }
    }
    if (minimumOwnTime === undefined) {
      throw new Error('Operation not producible')
    }
    // add longest predecessor
    let longestPredecessor = 0;
    for (const p of this.predecessors) {
      const time = p.operation.getMinimumRequiredRemainingProductionTimeAfterThisAndPreviousOperations(schedulingProblem, excludedOperation);
      if (time > longestPredecessor) {
        longestPredecessor = time;
      }
    }
    // return
    return minimumOwnTime + longestPredecessor;
  }

  getProducibleOperations(): Operation[] {
    // own producible
    if (this.isProducible()) {
      return [this];
    }
    // predecessors
    let ans: Operation[] = [];
    for (const p of this.predecessors) {
      ans = ans.concat(p.operation.getProducibleOperations());
    }
    return ans;
  }

  private isProducible(): boolean {
    // no predecessors
    if(this.predecessors.length !== 0) return false;
    return true;
  }

  getNextProductionAidsAvailableSlot(duration: number, after: number): StartEnd {
    let slot:StartEnd = {start: after, end: after+duration-1};
    let allSatisfied = true;
    // loop until all conditions met
    do {
      allSatisfied = true;
      // loop over all production aids
      for (const pa of this.requiredProductionAids) {
        const ans = pa.productionAid.getNextUsageSlot(pa.amount, duration, slot.start);
        if (ans.start !== slot.start) {
          allSatisfied = false;
          slot = ans;
          break;
        }
      }
    } while (!allSatisfied)
    // return
    return slot;
  }

  /**
   * updates all variables after this operation is planned
   */
  processPlannedOperation(slot: StartEnd): void {
    if (this.job === undefined) throw new Error('Operation: job not set')
    // call function on job
    this.job.removePredecessorAndUpdateAvailable(this, slot.end + 1);
    // assign production aids
    for (const aid of this.requiredProductionAids) {
      aid.productionAid.addAssignment(this, slot, aid.amount);
    }
  }

  removePredecessorAndUpdateAvailable(operation: Operation, availableAt: number): void {
    // process here
    // check if direct predecessor exists
    for (const o of this.predecessors) {
      if (o.operation === operation) { // found, remove and update next available here
        this.predecessors.splice(this.predecessors.indexOf(o), 1);
        if (this.availableAt < availableAt) this.availableAt = availableAt;
      }
    }
    // pass to all children
    for (const o of this.predecessors) {
      o.operation.removePredecessorAndUpdateAvailable(operation, availableAt);
    }
  }
}

export interface OperationPredecessor {
  operation: Operation;
  successorHasNoWaitCondition: boolean;
}

export interface RequiredProductionAid {
  productionAid: ProductionAid;
  amount: number;
}
import Container from "typedi";
import { FunctionProviderEvaluatorService } from "../../function-evaluator/function-provider-evaluator.service";
import { FunctionProviderVariables } from "../../function-evaluator/function-provider-variables.class";
import { FunctionProvider } from "../../function-evaluator/function-provider.interface";
import { ScheduleResDto, ScheduleResDtoProductionResource } from "../../response-data-transfer-objects/schedule.res-dto";
import { ProductionBuffer } from "./buffer.class";
import { Job } from "./job.class";
import { Material } from "./material.class";
import { Operation } from "./operation.class";
import { ProductionAid } from "./production-aid.class";
import { ProductionResource } from "./production-resource.class";
import { SimultaneousProductionCondition } from "./simultaneous-production-condition.class";

export class SchedulingProblem {

  goalValue: number|undefined;

  constructor(
    public goalFunction: FunctionProvider,
    public jobs: Job[],
    public materials: Material[],
    public simultaneousProductionConditions: SimultaneousProductionCondition[],
    public productionResources: ProductionResource[],
    public productionAids: ProductionAid[],
    public buffers: ProductionBuffer[],
  ) {
    for (const j of jobs) {
      j.schedulingProblem = this;
    }
    for (const pr of productionResources) {
      pr.schedulingProblem = this;
    }
  }

  getAllProducibleOperations(): Operation[] {
    let tmp: Operation[] = [];
    for (const p of this.jobs) {
      tmp = tmp.concat(p.getProducibleOperations());
    }
    // clean array
    let ans: Operation[] = [];
    for (const t of tmp) {
      if (!ans.includes(t)) {
        ans.push(t);
      }
    }
    return ans;
  }

  calculateGoalValue(): number {
    // check if already calculated
    if (this.goalValue !== undefined) {
      return this.goalValue;
    }
    // get goal variables
    const variables = this.getGoalVariables();
    // caulculate
    this.goalValue = Container.get(FunctionProviderEvaluatorService).evaluateFunction(this.goalFunction, variables);
    // return
    return this.goalValue;
  }

  private getGoalVariables(): FunctionProviderVariables {
    // init counters
    let totalMaximumMakespan = 0;
    let totalCosts = 0;
    let totalJobsInTime = 0;
    let totalChangeoverTime = 0;
    // loop over all assignments
    for (const pr of this.productionResources) {
      for (const a of pr.assignments) {
        // makespan
        if (a.endTime > totalMaximumMakespan) {
          totalMaximumMakespan = a.endTime;
        }
        // total Costs
        totalCosts += a.cost;
        // changeover time
        if (a.type === 'changeover') {
          totalChangeoverTime += a.endTime - a.startTime + 1;
        }
      }
    }
    // loop over all jobs
    for (const j of this.jobs) {
      // jobs in time
      if (j.finishedAt === undefined) {
        throw new Error('Job has no finished at set');
      }
      if (j.finishedAt <= j.dueDate) {
        totalJobsInTime++;
      }
    }
    // build anser
    const ans = new FunctionProviderVariables();
    ans.totalMaximumMakespan = totalMaximumMakespan;
    ans.totalCosts = totalCosts;
    ans.timeliness = totalJobsInTime / this.jobs.length;
    ans.totalChangeoverTime = totalChangeoverTime;
    // return
    return ans;
  }

  convertToRes(): ScheduleResDto {
    // build production resources
    const prs: ScheduleResDto['productionResources'] = [];
    for (const pr of this.productionResources) {
      const as: ScheduleResDtoProductionResource['assignments'] = [];
      for (const a of pr.assignments) {
        if (a.type === 'operation') {
          as.push({
            type: 'operation',
            startTime: a.startTime,
            endTime: a.endTime,
            operationId: a.operation.id,
            materialId: a.material.id,
            cost: a.cost,
          })
        } else if (a.type === 'changeover') {
          as.push({
            type: 'changeover',
            startTime: a.startTime,
            endTime: a.endTime,
            fromMaterialId: a.fromMaterial.id,
            toMaterialId: a.toMaterial.id,
            cost: a.cost,
          })
        }
      }
      prs.push({
        productionResourceId: pr.id,
        assignments: as,
      })
    }
    // build jobs
    const js:ScheduleResDto['productionJobs'] = [];
    for (const j of this.jobs) {
      js.push({
        name: j.name,
        dueDate: j.dueDate,
        finishedAt: j.finishedAt as number,
      });
    }
    // buffers
    const buffers:ScheduleResDto['buffers'] = [];
    for (const b of this.buffers) {
      buffers.push({
        id: b.id,
        size: b.size,
        usages: b.usages,
      })
    }
    // production aids
    const aids: ScheduleResDto['productionAids'] = []
    for (const a of this.productionAids) {
      aids.push({
        id: a.id,
        amountAvailable: a.amountAvailable,
        usages: a.usages,
      })
    }
    // get goal value
    const goalValue = this.calculateGoalValue();
    // build and return object
    return {
      productionResources: prs,
      productionJobs: js,
      goalValue: goalValue,
      buffers: buffers,
      productionAids: aids,
    }
  }
}
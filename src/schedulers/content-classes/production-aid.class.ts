import { StartEnd } from "../../util/start-end.interface";
import { Operation } from "./operation.class";

export class ProductionAid {

  usages: ProductionAidUsage[] = []

  constructor(
    public id: string,
    public amountAvailable: number,
  ) {}

  getNextUsageSlot(amount: number, duration: number, after: number): StartEnd {
    // failsave
    if (amount > this.amountAvailable) throw new Error('too many production aids requested');
    // check if possible and store next reduction time
    let totalUsed = 0;
    let earliestReductionEndTime: number|undefined;
    for (const u of this.usages) {
      if (u.startTime >= after+duration || u.endTime < after) continue // out of time
      totalUsed += amount;
      if (!earliestReductionEndTime || earliestReductionEndTime > u.endTime) earliestReductionEndTime = u.endTime;
    }
    // return if possible, else return recursive call
    if (totalUsed + amount <= this.amountAvailable) {
      return {start: after, end: after + duration -1}
    } else {
      if (earliestReductionEndTime === undefined) throw new Error('Logic error getNextUsageSlot')
      return this.getNextUsageSlot(amount, duration, earliestReductionEndTime+1)
    }
  }

  addAssignment(operation: Operation, slot: StartEnd, amount: number): void {
    this.usages.push({
      byOperationId: operation.id,
      amount: amount,
      startTime: slot.start,
      endTime: slot.end,
    })
  }
}

export interface ProductionAidUsage {
  byOperationId: string;
  amount: number
  startTime: number
  endTime: number
}
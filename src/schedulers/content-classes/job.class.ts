import { Operation } from "./operation.class";
import { SchedulingProblem } from "./scheduling-problem.class";

export class Job {

  schedulingProblem: SchedulingProblem|undefined;

  finishedAt: number|undefined;

  constructor(
    public name: string,
    public dueDate: number,
    public priority: number,
    public operations: Operation[],
  ) {
    for (const o of operations) {
      o.job = this;
    }
  }

  getMinimumRequiredRemainingProductionTime(excludedOperation: Operation): number {
    if (this.schedulingProblem === undefined) throw new Error('scheduling problem not set in job')
    // find longest operation
    let longestOperation: number = 0;
    for (const o of this.operations) {
      const time = o.getMinimumRequiredRemainingProductionTimeAfterThisAndPreviousOperations(this.schedulingProblem, excludedOperation);
      if (time > longestOperation) {
        longestOperation = time;
      }
    }
    return longestOperation;
  }

  getProducibleOperations(): Operation[] {
    let ans: Operation[] = [];
    for (const p of this.operations) {
      ans = ans.concat(p.getProducibleOperations());
    }
    return ans;
  }

  removePredecessorAndUpdateAvailable(operation: Operation, availableAt: number): void {
    // update direct children
    for (const o of this.operations) {
      if (o === operation) {
        this.operations.splice(this.operations.indexOf(o), 1);
        if (this.finishedAt === undefined || availableAt-1 > this.finishedAt) {
          this.finishedAt = availableAt - 1;
        }
      }
    }
    // pass to operations
    for (const o of this.operations) {
      o.removePredecessorAndUpdateAvailable(operation, availableAt);
    }
  }
}
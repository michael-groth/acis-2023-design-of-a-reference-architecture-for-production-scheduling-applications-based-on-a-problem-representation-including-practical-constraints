
export class ProductionBuffer {

  usages: BufferUsage[] = [];

  constructor(
    public id: string,
    public size: number,
  ) {}

  getCurrentRelativeFillAt(moment: number): number {
    let fill = 0;
    for (const u of this.usages) {
      if(u.startTime <= moment) {
        if (u.endTime === undefined || u.endTime >= moment) {
          fill += u.amount;
        }
      }
    }
    return fill/this.size;
  }

  addStartUsage(materialId: string, fromOperationId: string, amount: number, startTime: number): void {
    this.usages.push({
      materialId,
      fromOperationId,
      amount,
      startTime,
    })
  }

  addEndUsage(materialId: string, amount: number, endTime: number, toOperationId: string): void {
    for (const u of this.usages) {

      if (u.materialId === materialId && u.amount === amount && u.endTime === undefined) {
        // found, add end
        u.endTime = endTime;
        u.toOperationId = toOperationId;
        return
      }
    }
  }

  hasSpaceAtSomePointFor(amount: number):boolean {
    if (amount <= 0) return true;
    let fillInfinite = 0;
    for (const u of this.usages) {
      if (u.endTime === undefined) {
        fillInfinite += u.amount;
      }
    }
    return (fillInfinite + amount <= this.size);
  }

  getNextAvailableSlot(amount: number, after: number): number {
    // failsave
    if (amount > this.size) throw new Error('too much buffer size requested');
    // check if possible and store next reduction time
    let totalUsed = 0;
    let earliestReductionEndTime: number|undefined;
    for (const u of this.usages) {
      // no usage
      if (u.amount <= 0) continue;
      // out of time
      if (u.endTime < after) continue;
      // calculations
      totalUsed += amount;
      if (u.endTime !== undefined && (earliestReductionEndTime === undefined || earliestReductionEndTime > u.endTime)) earliestReductionEndTime = u.endTime;
    }
    // return if possible, else return recursive call
    if (totalUsed + amount <= this.size) {
      return after
    } else {
      if (earliestReductionEndTime === undefined) throw new Error('Logic error getNextAvailableSlot')
      return this.getNextAvailableSlot(amount, earliestReductionEndTime+1)
    }
  }
}


export interface BufferUsage {
  materialId: string
  fromOperationId: string;
  amount: number
  startTime: number
  endTime?: number
  toOperationId?: string
}
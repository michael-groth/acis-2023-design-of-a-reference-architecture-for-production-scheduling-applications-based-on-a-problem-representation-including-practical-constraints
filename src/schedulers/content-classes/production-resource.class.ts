import Container from "typedi";
import { FunctionProviderEvaluatorService } from "../../function-evaluator/function-provider-evaluator.service";
import { FunctionProviderVariables } from "../../function-evaluator/function-provider-variables.class";
import { FunctionProvider } from "../../function-evaluator/function-provider.interface";
import { SimultaneousProductionConditionType } from "../../request-data-transfer-objects/simultaneous-production-condition.req-dto";
import { StartEnd } from "../../util/start-end.interface";
import { TimeCost } from "../../util/time-cost.interface";
import { ProductionBuffer } from "./buffer.class";
import { Material } from "./material.class";
import { Operation } from "./operation.class";
import { SchedulingProblem } from "./scheduling-problem.class";
import { SimultaneousProductionCondition } from "./simultaneous-production-condition.class";

export class ProductionResource {

  assignments: ResourceAssignment[] = []
  nextAvailable: number = 0;
  schedulingProblem: SchedulingProblem|undefined;
  
  constructor(
    public id: string,
    public producibleMaterials: {
      materialGroup: string;
      timeFunction: FunctionProvider;
      costFunction: FunctionProvider;
    }[],
    public bufferBefore: ProductionBuffer,
    public bufferAfter: ProductionBuffer,
    public changeovers: {
      fromMaterialGroup: string;
      toMaterialGroup: string;
      timeFunction: FunctionProvider;
      costFunction: FunctionProvider;
    }[],
    public fixedAssignments: {
      fromTime: number;
      toTime: number;
      /** defines if a started operation on this production resource can be split by this fixed assignment. It means: 0-100: Operation A starts, 101-200: fixed Assignment, 201-300: Operation A finishes */
      startedOperationsCanBeSplit: boolean;
    }[],
    public currentSetupMaterial: Material,
    public simultaneousProductionConditions: SimultaneousProductionCondition[],
  ) {}

  /** 
   * if multiple possible results exist, the first applicable is chosen
   */
  private getMaterialGroupsTimeCost(materialGroups: string[], amount: number): TimeCost {
    for (const mg of materialGroups) {
      for (const pm of this.producibleMaterials) {
        if (pm.materialGroup === mg) {
          const evaluator = Container.get(FunctionProviderEvaluatorService);
          const vars = new FunctionProviderVariables();
          vars.amount = amount;
          const time = evaluator.evaluateFunction(pm.timeFunction, vars)
          const cost = evaluator.evaluateFunction(pm.costFunction, vars)
          return {time, cost}
        }
      }
    }
    throw new Error(`materialGroup ${materialGroups} not defined`);
  }

  private canProduceMaterial(material: Material): boolean {
    for (const pm of this.producibleMaterials) {
      if(material.materialGroups.includes(pm.materialGroup)) {
        return true;
      }
    }
    return false;
  }

  /** only in general  */
  canProduceOperation(operation: Operation): boolean {
    for (const pm of operation.producesMaterial) {
      if (this.canProduceMaterial(pm)) {
        return true;
      }
    }
    return false;
  }

  /** is there any point in the future where it is available? */
  isBufferAfterAvailableToProduce(operation: Operation): boolean {
    const material = this.getProducedMaterial(operation);
    return this.bufferAfter.hasSpaceAtSomePointFor(material.bufferUsage * operation.amount) 
  }

  getOperationTimeCost(operation: Operation): TimeCost {
    const producibleMaterialGroups: string[] = [];
    for (const pm of operation.producesMaterial) {
      for (const mg of pm.materialGroups) {
        if (!producibleMaterialGroups.includes(mg)) {
          producibleMaterialGroups.push(mg);
        }
      }
    }
    return this.getMaterialGroupsTimeCost(producibleMaterialGroups, operation.amount)
  }

  getAlternativeProductionResourcesForOperation(operation: Operation): ProductionResource[] {
    if (this.schedulingProblem === undefined) throw new Error('ProductionResource: scheduling problem not set')
    const ans: ProductionResource[] = [];
    for (const pr of this.schedulingProblem.productionResources) {
      if (pr === this) continue;
      if (pr.canProduceOperation(operation)) ans.push(pr);
    }
    return ans;
  }

  getAlternativeOperationsOnThisProductionResource(excludedOperation: Operation): Operation[] {
    if (this.schedulingProblem === undefined) throw new Error('ProductionResource: scheduling problem not set')
    const producibleOperations = this.schedulingProblem.getAllProducibleOperations();
    const ans: Operation[] = [];
    for (const o of producibleOperations) {
      if (excludedOperation === o) continue;
      if (this.canProduceOperation(o)) {
        ans.push(o);
      }
    }
    return ans;
  }

  getChangeoverTimeCostForOperation(operation: Operation): TimeCost {
    // try to find
    for (const c of this.changeovers) {
      if (!this.currentSetupMaterial.materialGroups.includes(c.fromMaterialGroup)) continue;
      if (!operation.producesMaterial.some(m => m.materialGroups.includes(c.toMaterialGroup))) continue;
      const evaluator = Container.get(FunctionProviderEvaluatorService);
      return {time: evaluator.evaluateFunction(c.timeFunction), cost: evaluator.evaluateFunction(c.costFunction)};
    }
    // default return
    return {time: 0, cost: 0}
  }

  private getProducedMaterial(operation: Operation): Material {
    for (const m of operation.producesMaterial) {
      if (this.canProduceMaterial(m)) return m;
    }
    throw new Error('Material not producible');
  }

  getBufferAfterRelativeFillAfterProducing(operation: Operation, endTime: number): number {
    const m = this.getProducedMaterial(operation);
    return this.bufferAfter.getCurrentRelativeFillAt(endTime) + m.bufferUsage*operation.amount/ this.bufferAfter.size;
  }
  getBufferBeforeRelativeFillAfterProducing(operation: Operation, startTime: number): number {
    return this.bufferBefore.getCurrentRelativeFillAt(startTime) - operation.amount*operation.madeFromMaterial.bufferUsage/this.bufferBefore.size;
  }

  getRelativeAmountOfExcludedOtherOperationsIfScheduling(operation: Operation, alternativeOperationsOnThisProductionResource?: Operation[]): number {
    // easy decisions
    if (this.simultaneousProductionConditions.length === 0) return 0;
    // fill vars
    if (!alternativeOperationsOnThisProductionResource) alternativeOperationsOnThisProductionResource = this.getAlternativeOperationsOnThisProductionResource(operation);
    // calculate exludions
    let numExclusions = 0;
    const producedMaterial = this.getProducedMaterial(operation);
    for (const a of alternativeOperationsOnThisProductionResource) {
      const alternativeProducedMaterial = this.getProducedMaterial(a);
      if (!this.canProduceAtTheSameTime(producedMaterial.materialGroups, alternativeProducedMaterial.materialGroups)) numExclusions++;
    }
    // return
    return numExclusions / alternativeOperationsOnThisProductionResource.length;
  }

  private canProduceAtTheSameTime(materialGroups1: string[], materialGroups2: string[]): boolean {
    for (const c of this.simultaneousProductionConditions) {
      if (c.type === SimultaneousProductionConditionType.MATERIAL_EXCLUSIONS && c.value1 && c.value2) {
        if (
          (materialGroups1.includes(c.value1) && materialGroups2.includes(c.value2)) ||
          (materialGroups1.includes(c.value2) && materialGroups2.includes(c.value1))
        ) {
          return false;
        }
      }
    }
    return true;
  }

  getStartTimeOf(operation: Operation, includingChangeover: boolean): number {
    let highest = 0;
    // this resource ready + changeover
    if (includingChangeover) {
      const changeover = this.getChangeoverTimeCostForOperation(operation);
      if (this.nextAvailable + changeover.time > highest) highest = this.nextAvailable + changeover.time;
    } else {
      if (this.nextAvailable > highest) highest = this.nextAvailable;
    }
    
    
    // operation becoming available
    if (operation.availableAt > highest) highest = operation.availableAt;
    // simultaneous production slots and production aids
    highest = this.getNextSimultaneousAndAidAndBufferStartTime(operation, highest);
    // return
    return highest;
  }

  private getNextSimultaneousAndAidAndBufferStartTime(operation: Operation, after: number, timeCost?: TimeCost): number {
    // init
    let countRelevant = 0;
    let laterChanged = false;
    // simultaneous production slots
    if (this.simultaneousProductionConditions.length > 0) {
      countRelevant++;
      if (!timeCost) timeCost = this.getOperationTimeCost(operation);
      const simultaneousSlot = this.getNextSimultaneousSlotFor(operation, timeCost.time, after);
      if (simultaneousSlot.start > after) {
        after = simultaneousSlot.start;
        if (countRelevant >= 2) {
          laterChanged = true;
        }
      }
    }
    // production aids
    if (operation.requiredProductionAids.length > 0) {
      countRelevant++;
      if (!timeCost) timeCost = this.getOperationTimeCost(operation);
      const productionAidSlot = operation.getNextProductionAidsAvailableSlot(timeCost.time, after);
      if (productionAidSlot.start > after) {
        after = productionAidSlot.start;
        if (countRelevant >= 2) {
          laterChanged = true;
        }
      }
    }
    // buffer
    const requiredBufferSize = this.getProducedMaterial(operation).bufferUsage * operation.amount;
    if (requiredBufferSize > 0) {
      countRelevant++;
      if (!timeCost) timeCost = this.getOperationTimeCost(operation);
      const bufferSlot = this.bufferAfter.getNextAvailableSlot(requiredBufferSize, after + timeCost.time)
      if (bufferSlot > after + timeCost.time) {
        after = bufferSlot - timeCost.time;
        if (countRelevant >= 2) {
          laterChanged = true;
        }
      }
    }
    // return
    if (!laterChanged) {
      return after;
    } else {
      return this.getNextSimultaneousAndAidAndBufferStartTime(operation, after, timeCost);
    }
  }

  private getNextSimultaneousSlotFor(operation: Operation, duration: number, after: number): StartEnd {
    // check if possible and store next end time
    let earliestEndTime: number|undefined;
    // create list of all relevant production resources to check
    const relevantOtherProductionResourceIds: string[] = [];
    for (const c of this.simultaneousProductionConditions) {
      for (const m of c.productionResourceIds) {
        if (!relevantOtherProductionResourceIds.includes(m) && m !== this.id) {
          relevantOtherProductionResourceIds.push(m);
        }
      }
    }
    // loop over all relevant production resources and check assignments
    if (this.schedulingProblem === undefined) throw new Error('ProductionResource: scheduling problem not set')
    for (const pr of this.schedulingProblem.productionResources) {
      if (!relevantOtherProductionResourceIds.includes(pr.id)) continue;
      for (const a of pr.assignments) {
        if (a.startTime >= after+duration || a.endTime < after) continue; // out of time
        // check for exclusion criteria
        if (a.type === 'operation') {
          if (!this.canProduceAtTheSameTime(this.getProducedMaterial(operation).materialGroups, this.getProducedMaterial(a.operation).materialGroups))   {
            // not possible at the same time, block during this
            earliestEndTime = a.endTime;
          }
        }
      }
    }
    // return in possible, else return recursive call
    if (earliestEndTime === undefined) {
      return { start: after, end: after + duration - 1 }
    } else {
      return this.getNextSimultaneousSlotFor(operation, duration, earliestEndTime + 1);
    }
  }

  addAssignment(operation: Operation): StartEnd { 
    // load data
    const timeCost = this.getOperationTimeCost(operation);
    const changeoverTimeCost = this.getChangeoverTimeCostForOperation(operation);
    const startTime = this.getStartTimeOf(operation, false);
    const producedMaterial = this.getProducedMaterial(operation);
    // add changeover
    if (changeoverTimeCost.time > 0 || changeoverTimeCost.cost > 0) {
      this.assignments.push({
        type: 'changeover',
        startTime: startTime,
        endTime: startTime + changeoverTimeCost.time - 1,
        fromMaterial: this.currentSetupMaterial,
        toMaterial: producedMaterial,
        cost: changeoverTimeCost.cost,
      });
    }
    // add assignment
    this.assignments.push({
      type: 'operation',
      startTime: startTime + changeoverTimeCost.time,
      endTime: startTime + changeoverTimeCost.time + timeCost.time - 1,
      operation: operation,
      material: producedMaterial,
      cost: timeCost.cost,
    })
    // update available (if required according to simultaneous production slots)
    this.nextAvailable = startTime + changeoverTimeCost.time + timeCost.time + operation.minimumStoragePeriod;
    this.currentSetupMaterial = producedMaterial;
    // update buffer before
    this.bufferBefore.addEndUsage(operation.madeFromMaterial.id, operation.madeFromMaterial.bufferUsage * operation.amount, startTime -1, operation.id);
    // update buffer after
    this.bufferAfter.addStartUsage(producedMaterial.id, operation.id, producedMaterial.bufferUsage * operation.amount, startTime + changeoverTimeCost.time + timeCost.time);
    // return
    return {
      start: startTime + changeoverTimeCost.time,
      end: startTime + changeoverTimeCost.time + timeCost.time -1,
    }
  }

}

export type ResourceAssignment = ResourceAssignmentOperation | ResourceAssignmentChangeover;

export interface ResourceAssignmentOperation {
  type: 'operation',
  startTime: number;
  endTime: number;
  operation: Operation; 
  material: Material;
  cost: number;
}
export interface ResourceAssignmentChangeover {
  type: 'changeover';
  startTime: number;
  endTime: number;
  fromMaterial: Material;
  toMaterial: Material;
  cost: number;
}

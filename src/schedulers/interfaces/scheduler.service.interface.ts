import { SchedulingProblemReqDto } from "../../request-data-transfer-objects/scheduling-problem.req-dto";
import { ScheduleResDto } from "../../response-data-transfer-objects/schedule.res-dto";

export interface SchedulerService {

  generateScheduling(schedulingProblem: SchedulingProblemReqDto): Promise<ScheduleResDto>

  updateTrainingData?(schedulingProblem: SchedulingProblemReqDto): void
}

import { BufferUsage } from "../schedulers/content-classes/buffer.class";
import { ProductionAidUsage } from "../schedulers/content-classes/production-aid.class";

export interface ScheduleResDto {
  productionResources: ScheduleResDtoProductionResource[]

  productionJobs: {
    name: string;

    dueDate: number;

    finishedAt: number;
  }[]

  buffers: ScheduleResDtoBuffer[];

  productionAids: ScheduleResDtoProductionAid[];

  goalValue: number;
}

export interface ScheduleResDtoProductionResource {
  productionResourceId: string;
  assignments: ScheduleResDtoResourceAssignment[]
}

export type ScheduleResDtoResourceAssignment = ScheduleResDtoResourceAssignmentOperation | ScheduleResDtoResourceAssignmentChangeover;

export interface ScheduleResDtoResourceAssignmentOperation {
  type: 'operation',
  startTime: number;
  endTime: number;
  operationId: string; 
  materialId: string;
  cost: number;
}
export interface ScheduleResDtoResourceAssignmentChangeover {
  type: 'changeover';
  startTime: number;
  endTime: number;
  fromMaterialId: string;
  toMaterialId: string;
  cost: number;
}

export interface ScheduleResDtoBuffer {
  id: string;
  size: number;
  usages: BufferUsage[];
}

export interface ScheduleResDtoProductionAid {
  id: string;
  amountAvailable: number;
  usages: ProductionAidUsage[];
}
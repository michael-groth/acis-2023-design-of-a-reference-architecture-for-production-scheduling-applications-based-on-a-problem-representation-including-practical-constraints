import express from 'express';
import { scheduleRoutes } from './schedule/schedule.routes';
import { trainingRoutes } from './training/training.routes';

export const routes = express.Router()

routes.use('/schedule', scheduleRoutes);
routes.use('/training', trainingRoutes)



routes.get('', (req, res) => {
  res.send('default answer')
})
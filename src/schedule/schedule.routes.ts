import express, { Request, Response } from 'express';
import Container from 'typedi';
import { isSchedulingProblemReqDto } from '../request-data-transfer-objects/scheduling-problem.req-dto';
import { ScheduleController } from './schedule.controller';

export const scheduleRoutes = express.Router();

const controller = Container.get(ScheduleController);

scheduleRoutes.post('', async (req: Request, res: Response) => {
  try {
    if (!isSchedulingProblemReqDto(req.body)) {
      return res.status(400).send('Bad Request')
    }
    res.send(controller.getSchedule(req.body));
  } catch(e) {
    if (e instanceof Error) {
      res.status(500).send(e.message);
    } else {
      res.status(500).send(e)
    }
    
  }
})

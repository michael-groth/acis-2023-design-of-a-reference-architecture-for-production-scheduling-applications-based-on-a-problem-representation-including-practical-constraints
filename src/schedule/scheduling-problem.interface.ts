// probably not needed at all

// this file is only for the anonymized review process. After review, the link to this file will be exchanged with a git repository

export interface SchedulingProblem {
  /** a function describing the goal. It must be interpreted by the Evaluation Simulation Engine */
  goal: string;
  /** array of all jobs of the scheduling problem */
  jobs: ProductionJob[];
}

export interface ProductionJob {
  /** identifier of a production job */
  id: string;
  /** a value >=0 defining the priority. The higher the value, the higher the priority */
  priority: number;
  /** due date in the form of a number. The start of the schedule is time 0 */
  dueDate: number;
  /** array of all operations to complete the production job */
  operations: Operation[];
}

export interface Operation {
  /** identifier of an operation */
  id: string;
  /** the amount of material to be produced */
  amount: number;
  /** array of the predecessor ids that must be completed before this operation can be produced */
  predecessors: {
    /** the predecessing operation */
    operation: Operation;
    /** defines if the following operation must start immediately after the previous completed */
    hasNoWaitCondition: boolean;
  }[];
  /** array of the operations that cannot be produced at the same time */
  sameTimeExclusions: Operation[];
  /** array of alternative material ids that can be produced as a result of the operation */
  alternativelyProducedMaterials: Material[];
}

export interface Material {
  /** identifier of a material */
  id: string;
  /** amount of usage of buffers at production resources. Should only be set if required */
  bufferUsage?: number;
  /** array of production resources that can produce the material, with times and costs required for the production */
  producedOn: {
    /** production resource on which the material can be produced */
    productionResource: ProductionResource;
    /** a function to calculate the time needed for producing this material on the production resource */
    time: string;
    /** a function to calculate the costs needed for producing this material on the production resource */
    costs: string;
  }[];
}

export interface ProductionResource {
  /** identifier of a production resource */
  id: string;
  /** size of buffer before the production resource. Should only be set if required */
  bufferBefore?: number;
  /** size of buffer after the production resource. Should only be set if required */
  bufferAfter?: number;
  /** a function describing the chance of a failure of the production resource */
  failureChance: string;
  /** a function describing the chance of a deviation of the production resource */
  deviationChance: string;
  /** a function describing the effect of a deviation of the production resource */
  deviationEffect: string;
  /** array of the possible changeovers */
  changeovers: {
    /** function to calculate the time needed for the changeover */
    time: string;
    /** function to calculate the costs needed for the changeover */
    costs: string;
    /** the material that is produced after the changeover */
    to: Material;
    /** the material that was produced before the changeover. Only required for sequence-dependent changeovers. */
    from: Material;
  }[];
  /** define how many operations can be simultaneously produced on the resource. Usually, set to 1 */
  simultaneousProductionSlots: number;
  /** conditions that describe how simultaneous production works. Only set if required */
  simultaneousProductionConditions: {
    /** type of the condition */
    type: string;
    /** value depending on the type */
    value1?: string;
    value2?: string;
  }[];
}
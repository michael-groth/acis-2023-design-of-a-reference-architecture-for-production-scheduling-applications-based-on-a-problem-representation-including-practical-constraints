#  Production Scheduling Architecture Demonstration

The application privides a framework for production scheduling with practical constraints. The problem to be solved can be defined in the src/index.ts file. For continous scheduling of ad hoc problems, a REST API can be used. For that, the index.ts file has to be exchanged with the index-router.ts file and the route definition for the scheduler to be used has to be defined. 


## Local Development

We suggest the development with Visual Studio Code and the usage of Development containers as no dependecies must be installed locally using development containers. Otherwise, we follow the best practices of typescript development.